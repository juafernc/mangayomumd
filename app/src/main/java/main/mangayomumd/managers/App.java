package main.mangayomumd.managers;

import android.app.Application;
import android.content.Context;

import com.activeandroid.ActiveAndroid;

/**
 * Created by juanma on 29/10/15.
 */
public class App extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        // Inicializar Active Android
        ActiveAndroid.initialize(this);
        // Obtener el contexto de la aplicación
        App.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return App.context;
    }
}
