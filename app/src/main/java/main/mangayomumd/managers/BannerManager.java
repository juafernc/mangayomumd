package main.mangayomumd.managers;

import android.app.Activity;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import main.mangayomumd.R;

/**
 * Created by juanma on 2/12/15.
 */
public class BannerManager {
    public static void createBanner(View view) {
        AdView mAdView = (AdView) view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("54706E6573B85749E1878759B5B7B05D")
                .build();
        mAdView.loadAd(adRequest);
    }
}
