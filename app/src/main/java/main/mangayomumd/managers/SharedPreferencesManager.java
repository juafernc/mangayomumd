package main.mangayomumd.managers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by juanma on 29/10/15.
 */
public class SharedPreferencesManager {

    private static SharedPreferencesManager sharedPreferencesManager;

    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor sharedPreferencesEditor;

    public SharedPreferencesManager() {
        sharedPreferences = App.getAppContext().getSharedPreferences("main.MangaYomu", Context.MODE_PRIVATE);
        sharedPreferencesEditor = sharedPreferences.edit();
    }

    public static SharedPreferencesManager getInstance() {
        if (sharedPreferencesManager == null)
            sharedPreferencesManager = new SharedPreferencesManager();
        return sharedPreferencesManager;
    }

    public Boolean getBoolean(String type) {
        return sharedPreferences.getBoolean(type, false);
    }

    public void setBoolean(String type, Boolean isSelect) {
        sharedPreferencesEditor.putBoolean(type, isSelect).apply();
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public void setString(String key, String value) {
        sharedPreferencesEditor.putString(key, value).apply();
    }

    public Integer getInteger(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public void setInteger(String key, Integer value) {
        sharedPreferencesEditor.putInt(key, value).apply();
    }

    public Long getLong(String key) {
        return sharedPreferences.getLong(key, 0);
    }

    public void setLong(String key, Long value) {
        sharedPreferencesEditor.putLong(key, value).apply();
    }

}
