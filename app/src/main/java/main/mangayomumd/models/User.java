package main.mangayomumd.models;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by juanma on 31/10/15.
 */
@Table(name = "User")
public class User extends Model {

    @Column(name = "Email", index = true)
    public String email;

    @Column(name = "UserName", index = true)
    public String user_name;

    @Column(name = "ClientId")
    public String client_id;

    @Column(name = "NextRandom")
    public String next_random;

    @Column(name = "Pin")
    public String pin;

}
