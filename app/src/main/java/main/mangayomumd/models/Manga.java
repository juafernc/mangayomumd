package main.mangayomumd.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

/**
 * Created by juanma on 29/10/15.
 */
@Table(name = "Manga")
public class Manga extends Model implements Parcelable {

    @Column(name = "Name", index = true)
    public String name;

    @Column(name = "ServerName", index = true)
    public String server_name;

    @Column(name = "UrlImage")
    public String url_image;

    @Column(name = "UrlServerManga")
    public String url_server_manga;

    @Column(name = "Description")
    public String description;

    @Column(name = "Ranking")
    public Integer ranking;

    @Column(name = "Favourite")
    public Boolean favourite;

    @Column(name = "ListChaptersString")
    public String listChaptersString;

    public List<String> list_chapters;

    public Manga() {
        super();
    }

    public Manga(String name, String server_name, String url_image, String url_server_manga, String description, Integer ranking, Boolean favourite) {
        super();
        this.name = name;
        this.server_name = server_name;
        this.url_image = url_image;
        this.url_server_manga = url_server_manga;
        this.description = description;
        this.ranking = ranking;
        this.favourite = favourite;
    }

    @Override
    public String toString() {
        return String.format("name: %s, server_name: %s, url_image: %s, url_server_name: %s, description: %s, ranking: %d, favourite: %s, list_chapters: %s",
                name, server_name, url_image, url_server_manga, description, (ranking != null)? ranking.intValue():-1, (favourite != null)? favourite? "yes":"no" : "no", list_chapters.toString());
    }

    /**
     * Serializar y deserializar lista
     * */

    public void serialize() {
        Gson gson = new Gson();
        if (list_chapters != null)
            listChaptersString = gson.toJson(list_chapters);
    }

    public void deserialize() {
        Gson gson = new Gson();
        list_chapters = gson.fromJson(listChaptersString, new TypeToken<List<String>>(){}.getType());
    }


    /**
     * PARCELABLE
     * */

    protected Manga(Parcel in) {
        name = in.readString();
        server_name = in.readString();
        url_image = in.readString();
        url_server_manga = in.readString();
        description = in.readString();
        ranking = in.readInt();
        boolean[] temp = new boolean[1];
        in.readBooleanArray(temp);
        favourite = temp[0];
        listChaptersString = in.readString();
    }

    public static final Creator<Manga> CREATOR = new Creator<Manga>() {
        @Override
        public Manga createFromParcel(Parcel in) {
            return new Manga(in);
        }

        @Override
        public Manga[] newArray(int size) {
            return new Manga[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(server_name);
        dest.writeString(url_image);
        dest.writeString(url_server_manga);
        dest.writeString(description);
        dest.writeInt(ranking);
        if (favourite != null)
            dest.writeBooleanArray(new boolean[]{favourite});
        else
            dest.writeBooleanArray(new boolean[]{false});
        dest.writeString(listChaptersString);
    }


    /**
     * OPERACIONES CON LA BASE DE DATOS
     * */

    public static Manga getManga(String serverName, String mangaName) {
        return new Select().from(Manga.class).where("ServerName = ? and Name = ?", serverName, mangaName).executeSingle();
    }

    public void update() {
        Manga mangaDB = Manga.getManga(server_name, name);
        if (mangaDB != null) {
            mangaDB.url_image = url_image;
            mangaDB.url_server_manga = url_server_manga;
            mangaDB.description = description;
            mangaDB.ranking = ranking;
            mangaDB.favourite = favourite;
            mangaDB.serialize();
            mangaDB.save();
        }
        else {
            serialize();
            save();
        }
    }

    public static void remove(Manga manga){
        new Delete().from(Manga.class).where("ServerName = ? and Name = ?", manga.server_name, manga.name).execute();
    }

    public void remove(){
        new Delete().from(Manga.class).where("ServerName = ? and Name = ?", server_name, name).execute();
    }

    public static List<Manga> getFavourites() {
        return new Select().from(Manga.class).where("Favourite = ?", true).execute();
    }

}
