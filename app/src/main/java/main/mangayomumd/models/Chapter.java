package main.mangayomumd.models;

import android.database.sqlite.SQLiteFullException;
import android.os.Parcel;
import android.os.Parcelable;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

/**
 * Created by juanma on 29/10/15.
 */
@Table(name = "Chapter")
public class Chapter extends Model implements Parcelable {

    @Column(name = "Name", index = true)
    public String name;

    @Column(name = "ServerName", index = true)
    public String server_name;

    @Column(name = "MangaName", index = true)
    public String manga_name;

    @Column(name = "ListImagesString")
    public String listImagesString;

    public List<String> list_images;

    @Column(name = "Readed")
    public Boolean readed;

    @Column(name = "Downloaded")
    public Boolean downloaded;

    public Chapter() {
        super();
    }

    public Chapter(String name, String server_name, String manga_name, Boolean readed, Boolean downloaded) {
        super();
        this.name = name;
        this.server_name = server_name;
        this.manga_name = manga_name;
        this.readed = readed;
        this.downloaded = downloaded;
    }

    @Override
    public String toString() {
        return String.format("name: %s, server_name: %s, manga_name: %s, readed: %s, list_images: %s",
                name, server_name, manga_name, (readed != null)? readed?"yes":"no" : "no", list_images.toString());
    }

    public String getIdString() {
        return server_name+manga_name+name;
    }


    /**
     * Serializar y deserializar lista
     * */

    public void serialize() {
        Gson gson = new Gson();
        listImagesString = gson.toJson(list_images);
    }

    public void deserialize() {
        Gson gson = new Gson();
        list_images = gson.fromJson(listImagesString, new TypeToken<List<String>>(){}.getType());
    }


    /**
     * PARCELABLE
     * */

    protected Chapter(Parcel in) {
        name = in.readString();
        server_name = in.readString();
        manga_name = in.readString();
        listImagesString = in.readString();
        boolean[] temp = new boolean[1];
        in.readBooleanArray(temp);
        readed = temp[0];
        in.readBooleanArray(temp);
        downloaded = temp[0];
    }

    public static final Creator<Chapter> CREATOR = new Creator<Chapter>() {
        @Override
        public Chapter createFromParcel(Parcel in) {
            return new Chapter(in);
        }

        @Override
        public Chapter[] newArray(int size) {
            return new Chapter[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(server_name);
        dest.writeString(manga_name);
        dest.writeString(listImagesString);
        if (readed != null)
            dest.writeBooleanArray(new boolean[]{readed});
        else
            dest.writeBooleanArray(new boolean[]{false});
        if (downloaded != null)
            dest.writeBooleanArray(new boolean[]{downloaded});
        else
            dest.writeBooleanArray(new boolean[]{false});
    }


    /**
     * OPERACIONES CON LA BASE DE DATOS
     * */

    public static Chapter getChapter(String nameServer, String nameManga, String nameChapter) {
        return new Select().from(Chapter.class).where("ServerName = ? and MangaName = ? and Name = ?", nameServer, nameManga, nameChapter).executeSingle();
    }

    public static List<Chapter> getChapters(String nameServer, String nameManga) {
        return new Select().from(Chapter.class).where("ServerName = ? and Manganame = ?", nameServer, nameManga).execute();
    }

    public static List<Chapter> getChaptersDownloaded() {
        return new Select().from(Chapter.class).where("Downloaded = ?", true).execute();
    }

    public void update() throws SQLiteFullException {
        Chapter chapterDB = getChapter(server_name, manga_name, name);
        if (chapterDB != null) {
            chapterDB.list_images = list_images;
            chapterDB.readed = readed;
            chapterDB.downloaded = downloaded;
            chapterDB.serialize();
            chapterDB.save();
        }
        else {
            serialize();
            save();
        }
    }

    public static void remove(Chapter chapter) {
        new Delete().from(Chapter.class).where("ServerName = ? and MangaName = ? and Name = ?", chapter.server_name, chapter.manga_name, chapter.name).execute();
    }

    public void remove() {
        new Delete().from(Chapter.class).where("ServerName = ? and MangaName = ? and Name = ?", server_name, manga_name, name).execute();
    }

}
