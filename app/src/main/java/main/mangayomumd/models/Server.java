package main.mangayomumd.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by juanma on 29/10/15.
 */
@Table(name = "Server")
public class Server extends Model implements Parcelable {

    @Column(name = "Name", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String name;

    @Column(name = "Url")
    public String url;

    @Column(name = "ListMangasString")
    public String listMangasString;

    public List<String> list_mangas;

    public Server() {
        super();
    }

    public Server(String name, String url) {
        super();
        this.name = name;
        this.url = url;
    }

    @Override
    public String toString() {
        return String.format("name: %s, url: %s, mangas: %s", name, url, list_mangas.toString());
    }

    /**
     * Serializar y deserializar lista
     * */

    public void serialize() {
        Gson gson = new Gson();
        listMangasString = gson.toJson(list_mangas);
    }

    public void deserialize() {
        Gson gson = new Gson();
        list_mangas = gson.fromJson(listMangasString, new TypeToken<List<String>>(){}.getType());
    }


    /**
     * PARCELABLE
     * */

    protected Server(Parcel in) {
        name = in.readString();
        url = in.readString();
        listMangasString = in.readString();
    }

    public static final Creator<Server> CREATOR = new Creator<Server>() {
        @Override
        public Server createFromParcel(Parcel in) {
            return new Server(in);
        }

        @Override
        public Server[] newArray(int size) {
            return new Server[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(url);
        dest.writeString(listMangasString);
    }


    /**
     * OPERACIONES CON LA BASE DE DATOS
     * */

    public static List<Server> getServers() {
        return new Select().from(Server.class).execute();
    }

    public static Server getServer(String name) {
        return new Select().from(Server.class).where("Name = ?", name).executeSingle();
    }

    public void update() {
        Server serverDB = getServer(name);
        if (serverDB != null) {
            serverDB.url = url;
            serverDB.serialize();
            serverDB.save();
        }
        else {
            serialize();
            save();
        }
    }

    public void remove() {
        new Delete().from(Server.class).where("Name = ?", name).execute();
    }

}
