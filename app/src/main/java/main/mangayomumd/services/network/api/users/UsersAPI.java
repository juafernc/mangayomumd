package main.mangayomumd.services.network.api.users;

import main.mangayomumd.models.User;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by juanma on 30/10/15.
 */
public interface UsersAPI {
    String RESOURCE_USER = "/user";
    String RESOURCE_TEST_LOGIN = "/test_login";
    String RESOURCE_LOGOUT = "/logout";

    @FormUrlEncoded
    @POST(UsersAPI.RESOURCE_USER)
    void login(@Field("user_name") String user_name, @Field("mail") String mail, Callback<User> cb);

    @GET(UsersAPI.RESOURCE_TEST_LOGIN)
    void testLogin(@Query("client_id") String client_id, @Query("key") String key, Callback<User> cb);

    @GET(UsersAPI.RESOURCE_LOGOUT)
    void logout(@Query("client_id") String client_id, @Query("key") String key, Callback<User> cb);
}
