package main.mangayomumd.services.network.api.favourites;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.otto.Bus;

import java.lang.reflect.Modifier;
import java.util.List;

import main.mangayomumd.BuildConfig;
import main.mangayomumd.models.User;
import main.mangayomumd.services.ServiceBus;
import main.mangayomumd.services.network.api.favourites.events.ChapterModified;
import main.mangayomumd.services.network.api.favourites.events.ChaptersReadedLoaded;
import main.mangayomumd.services.network.api.favourites.events.FavouriteAdded;
import main.mangayomumd.services.network.api.favourites.events.FavouriteRemoved;
import main.mangayomumd.services.network.api.favourites.events.FavouritesLoaded;
import main.mangayomumd.services.network.api.favourites.events.errors.Unauthenticated;
import main.mangayomumd.services.network.api.favourites.events.errors.UserNotMatch;
import main.mangayomumd.services.network.api.utils.DefaultClient;
import main.mangayomumd.services.network.api.utils.WebServices;
import main.mangayomumd.services.network.errors.NetworkDisabled;
import main.mangayomumd.services.network.errors.UserNotExists;
import main.mangayomumd.services.network.utils.Net;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.android.AndroidLog;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * Created by juanma on 31/10/15.
 */
public class FavouritesService {
    private final FavouritesAPI api;
    private final Bus bus;

    private final String LOG_TAG = FavouritesService.class.getSimpleName();

    private static FavouritesService _instance = null;

    public static FavouritesService getInstance() {
        if(_instance == null)
            _instance = getInstance(getDefaultApi(), getDefaultBus());
        return _instance;
    }

    /**
     * Method for Dependecy injection
     * @param api api interface to be used
     * @param bus bus to be used
     * @return The UsersService
     */
    public static FavouritesService getInstance(FavouritesAPI api, Bus bus) {
        _instance = new FavouritesService(api, bus);
        return _instance;
    }

    private static Bus getDefaultBus() {
        return ServiceBus.serviceBus;
    }

    private static Gson getDefaultGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        return builder.create();
    }

    private static FavouritesAPI getDefaultApi() {
        RestAdapter.Builder builder = new RestAdapter.Builder().setEndpoint(WebServices.USERS_SERVER);
        builder.setClient(DefaultClient.getDefaultClient());
        builder.setConverter(new GsonConverter(getDefaultGson()));
        if(BuildConfig.DEBUG)
            builder.setLogLevel(RestAdapter.LogLevel.FULL).setLog(new AndroidLog("RetrofitLog"));
        return builder.build().create(FavouritesAPI.class);
    }

    //Instance
    public FavouritesService(FavouritesAPI api, Bus bus) {
        this.api = api;
        this.bus = bus;
    }


    /*
    * Llamadas
    * */

    public void addFavourite(String user, String client_id, String key, String server, String manga, List<String> chapter) {
        if (Net.checkConnection()) {
            api.addFavourite(user, client_id, key, server, manga, chapter, new Callback<User>() {
                @Override
                public void success(User user, Response response) {
                    bus.post(new FavouriteAdded(user));
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.w(LOG_TAG, error);
                    if (error.getKind() == RetrofitError.Kind.HTTP) {
                        switch (error.getResponse().getStatus()) {
                            case 400:
                                bus.post(new UserNotExists());
                                break;
                            case 401:
                                bus.post(new Unauthenticated());
                                break;
                            case 403:
                                bus.post(new UserNotMatch());
                                break;
                            default:
                                bus.post(error);
                        }
                    }
                }
            });
        }
        else {
            bus.post(new NetworkDisabled());
        }
    }

    public void getFavourites(String user, String client_id, String key, Boolean addreaded, Boolean addextrainfo) {
        if (Net.checkConnection()) {
            api.getFavourites(user, client_id, key, addreaded, addextrainfo, new Callback<FavouritesLoaded>() {
                @Override
                public void success(FavouritesLoaded favouritesLoaded, Response response) {
                    bus.post(favouritesLoaded);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.w(LOG_TAG, error);
                    if (error.getKind() == RetrofitError.Kind.HTTP) {
                        switch (error.getResponse().getStatus()) {
                            case 400:
                                bus.post(new UserNotExists());
                                break;
                            default:
                                bus.post(error);
                        }
                    }
                }
            });
        }
        else {
            bus.post(new NetworkDisabled());
        }
    }

    public void removeFavourite(String user, String client_id, String key, String server, String manga) {
        if (Net.checkConnection()) {
            api.removeFavourite(user, client_id, key, server, manga, new Callback<User>() {
                @Override
                public void success(User user, Response response) {
                    bus.post(new FavouriteRemoved(user));
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.w(LOG_TAG, error);
                    if (error.getKind() == RetrofitError.Kind.HTTP) {
                        switch (error.getResponse().getStatus()) {
                            case 400:
                                bus.post(new UserNotExists());
                                break;
                            case 401:
                                bus.post(new Unauthenticated());
                                break;
                            case 403:
                                bus.post(new UserNotMatch());
                                break;
                            default:
                                bus.post(error);
                        }
                    }
                }
            });
        }
        else {
            bus.post(new NetworkDisabled());
        }
    }

    public void getChaptersReaded(String user, String client_id, String key, String server, String manga) {
        if (Net.checkConnection()) {
            api.getChaptersReaded(user, client_id, key, server, manga, new Callback<ChaptersReadedLoaded>() {
                @Override
                public void success(ChaptersReadedLoaded chaptersReadedLoaded, Response response) {
                    bus.post(chaptersReadedLoaded);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.w(LOG_TAG, error);
                    if (error.getKind() == RetrofitError.Kind.HTTP) {
                        switch (error.getResponse().getStatus()) {
                            case 400:
                                bus.post(new UserNotExists());
                                break;
                            case 401:
                                bus.post(new Unauthenticated());
                                break;
                            case 403:
                                bus.post(new UserNotMatch());
                                break;
                            default:
                                bus.post(error);
                        }
                    }
                }
            });
        }
        else {
            bus.post(new NetworkDisabled());
        }
    }

    public void modifyChapterReaded(String user, String client_id, String key, String server, String manga, List<String> chapter, final String type) {
        if (Net.checkConnection()) {
            api.modifyChapterReaded(user, client_id, key, server, manga, chapter, type, new Callback<User>() {
                @Override
                public void success(User user, Response response) {
                    bus.post(new ChapterModified(user, type));
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.w(LOG_TAG, error);
                    if (error.getKind() == RetrofitError.Kind.HTTP) {
                        switch (error.getResponse().getStatus()) {
                            case 400:
                                bus.post(new UserNotExists());
                                break;
                            case 401:
                                bus.post(new Unauthenticated());
                                break;
                            case 403:
                                bus.post(new UserNotMatch());
                                break;
                            default:
                                bus.post(error);
                        }
                    }
                }
            });
        }
        else {
            bus.post(new NetworkDisabled());
        }
    }
}
