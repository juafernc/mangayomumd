package main.mangayomumd.services.network.api.favourites.events;

import java.util.List;

/**
 * Created by juanma on 31/10/15.
 */
public class FavouritesLoaded {
    public List<Favourite> list_mangas;
    public String next_random;

    public class Favourite {
        String server_name;
        String manga_name;
        Integer numchapters;
        String imgurl;
        List<String> list_capterh_readed;
    }
}
