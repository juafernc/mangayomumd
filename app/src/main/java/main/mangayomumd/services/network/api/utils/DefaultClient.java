package main.mangayomumd.services.network.api.utils;

import android.content.Context;
import android.util.Log;

import com.squareup.okhttp.Cache;
import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import main.mangayomumd.BuildConfig;
import main.mangayomumd.managers.App;
import retrofit.client.OkClient;

/**
 * Created by juanma on 30/10/15.
 */
public class DefaultClient {
    private static final long CACHE_SIZE = 1024 * 1024; //1MB

    private static OkClient _client;
    public static OkClient getDefaultClient() {
        if(_client == null)
            _client = buildClient();
        return _client;
    }

    private static OkClient buildClient() {
        Cache cache = new Cache(new File(App.getAppContext().getCacheDir(), "retrofitCache"), CACHE_SIZE);
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setCache(cache);
        okHttpClient.setConnectTimeout(10, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(10, TimeUnit.SECONDS);
        okHttpClient.setWriteTimeout(10, TimeUnit.SECONDS);
        if(BuildConfig.DEBUG) {
            okHttpClient.networkInterceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Response response = chain.proceed(chain.request());
                    Log.d("OkHttpLog", "Cache-Control: " + response.cacheControl().toString());
                    if (response.cacheResponse() != null)
                        Log.d("OkHttpLog", "Cache response");
                    if (response.networkResponse() != null)
                        Log.d("OkHttpLog", "Network response");
                    return response;
                }
            });
        }
        return new OkClient(okHttpClient);
    }
}