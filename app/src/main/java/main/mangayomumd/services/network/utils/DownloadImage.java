package main.mangayomumd.services.network.utils;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import main.mangayomumd.R;
import main.mangayomumd.managers.App;
import main.mangayomumd.utils.Re;

/**
 * Created by juanma on 9/11/15.
 */
public class DownloadImage {

    private String urlImage;
    private ImageView mIvImage;
    private File fich;

    public DownloadImage(File fount, String urlImage, ImageView mIvImage) {
        this.urlImage = urlImage;
        this.mIvImage = mIvImage;
        fich = new File(fount, Re.getFichFromUrl(urlImage));
    }

    public DownloadImage(File fount, String urlImage) {
        this.urlImage = urlImage;
        fich = new File(fount, Re.getFichFromUrl(urlImage));
    }

    public void download() {
        if (mIvImage != null) {
            Log.d("downloadImageCache", "Descarga y fijación de imagen: " + mIvImage);
            DownloadImageTask downloadImageTask = new DownloadImageTask();
            downloadImageTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
        else {
            Log.d("downloadImageCache", "Descarga de imagen");
            storeImage();
        }
    }

    private class DownloadImageTask extends AsyncTask {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            downloadImage();
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (mIvImage != null) {
                if (fich.exists()) {
                    Log.d("downloadImageCache", "Obtener imagen del fichero: " + fich);
                    Picasso.with(App.getAppContext()).load(fich).error(R.drawable.sin_imagen).into(mIvImage);
                }
                else {
                    Log.d("downloadImageCache", "No existe el fichero " + fich);
                    Picasso.with(App.getAppContext()).load(R.drawable.sin_imagen).into(mIvImage);
                }
            }
        }
    }

    private void downloadImage() {
        if (!fich.exists()) {
            storeImage();
        }
    }

    private void storeImage() {
        Log.d("downloadImageCache", "Intento de almacenar imagen");
        Bitmap bitmap = null;
        FileOutputStream fout = null;
        if (!fich.exists()) {
            try {
                bitmap = Picasso.with(App.getAppContext()).load(urlImage).get();
                fout = new FileOutputStream(fich);
                bitmap.compress(Bitmap.CompressFormat.WEBP, 100, fout);
                fout.flush();
                fout.close();
                if (fich.exists()) {
                    Log.d("downloadImageCache", "Imagen almacenada bien");
                } else {
                    Log.d("downloadImageCache", "Imagen almacenada mal");
                }
            } catch (IOException e) {
                e.printStackTrace();
                fich.delete();
            }
        }
    }
}
