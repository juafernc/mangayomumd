package main.mangayomumd.services.network.api.favourites.events;

import java.util.List;

/**
 * Created by juanma on 31/10/15.
 */
public class ChaptersReadedLoaded {
    public List<String> list_chapter_readed;
    public String next_random;

    public ChaptersReadedLoaded(List<String> list_chapter_readed, String next_random) {
        this.list_chapter_readed = list_chapter_readed;
        this.next_random = next_random;
    }
}
