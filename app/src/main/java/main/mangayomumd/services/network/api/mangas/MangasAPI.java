package main.mangayomumd.services.network.api.mangas;

import main.mangayomumd.models.Chapter;
import main.mangayomumd.models.Manga;
import main.mangayomumd.models.Server;
import main.mangayomumd.services.network.api.mangas.events.ServersLoaded;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by juanma on 31/10/15.
 */
public interface MangasAPI {
    String RESOURCE_SERVER = "/server";
    String RESOURCE_MANGA = "/manga";

    // TODO: implementar ETag en todas las llamadas

    @GET(MangasAPI.RESOURCE_SERVER)
    void getServers(Callback<ServersLoaded> cb);

    @GET(MangasAPI.RESOURCE_SERVER+"/{server}")
    void getServer(@Path("server") String server, Callback<Server> cb);

    @GET(MangasAPI.RESOURCE_MANGA+"/{server}/{manga}")
    void getManga(@Path("server") String server, @Path("manga") String manga, Callback<Manga> cb);

    @GET(MangasAPI.RESOURCE_MANGA+"/{server}/{manga}/{chapter}")
    void getChapter(@Path("server") String server, @Path("manga") String manga, @Path("chapter") String chapter, Callback<Chapter> cb);
}
