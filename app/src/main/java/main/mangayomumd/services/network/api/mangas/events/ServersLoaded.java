package main.mangayomumd.services.network.api.mangas.events;

import java.util.List;

/**
 * Created by juanma on 31/10/15.
 */
public class ServersLoaded {
    public List<String> list_servers;

    public ServersLoaded(List<String> list_servers) {
        this.list_servers = list_servers;
    }
}
