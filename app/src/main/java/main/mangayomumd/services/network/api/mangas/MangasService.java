package main.mangayomumd.services.network.api.mangas;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.otto.Bus;

import java.lang.reflect.Modifier;

import main.mangayomumd.BuildConfig;
import main.mangayomumd.models.Chapter;
import main.mangayomumd.models.Manga;
import main.mangayomumd.models.Server;
import main.mangayomumd.services.ServiceBus;
import main.mangayomumd.services.network.api.mangas.events.ChapterRow;
import main.mangayomumd.services.network.api.mangas.events.ServersLoaded;
import main.mangayomumd.services.network.api.mangas.events.errors.ChapterNotExists;
import main.mangayomumd.services.network.api.mangas.events.errors.MangaNotExists;
import main.mangayomumd.services.network.api.mangas.events.errors.ServerNotExists;
import main.mangayomumd.services.network.api.utils.DefaultClient;
import main.mangayomumd.services.network.api.utils.WebServices;
import main.mangayomumd.services.network.errors.NetworkDisabled;
import main.mangayomumd.services.network.utils.Net;
import main.mangayomumd.views.adapters.MangaListChaptersAdapter;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.android.AndroidLog;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

/**
 * Created by juanma on 31/10/15.
 */
public class MangasService {
    private final MangasAPI api;
    private final Bus bus;

    private final String LOG_TAG = MangasService.class.getSimpleName();

    private static MangasService _instance = null;

    public static MangasService getInstance() {
        if(_instance == null)
            _instance = getInstance(getDefaultApi(), getDefaultBus());
        return _instance;
    }

    /**
     * Method for Dependecy injection
     * @param api api interface to be used
     * @param bus bus to be used
     * @return The UsersService
     */
    public static MangasService getInstance(MangasAPI api, Bus bus) {
        _instance = new MangasService(api, bus);
        return _instance;
    }

    private static Bus getDefaultBus() {
        return ServiceBus.serviceBus;
    }

    private static Gson getDefaultGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        return builder.create();
    }

    private static MangasAPI getDefaultApi() {
        RestAdapter.Builder builder = new RestAdapter.Builder().setEndpoint(WebServices.USERS_SERVER);
        builder.setClient(DefaultClient.getDefaultClient());
        builder.setConverter(new GsonConverter(getDefaultGson()));
        if(BuildConfig.DEBUG)
            builder.setLogLevel(RestAdapter.LogLevel.FULL).setLog(new AndroidLog("RetrofitLog"));
        return builder.build().create(MangasAPI.class);
    }

    //Instance
    public MangasService(MangasAPI api, Bus bus) {
        this.api = api;
        this.bus = bus;
    }

    public MangasAPI getApi() {
        return api;
    }


    /*
    * Llamadas
    * */

    public void getServers() {
        if (Net.checkConnection()) {
            api.getServers(new Callback<ServersLoaded>() {
                @Override
                public void success(ServersLoaded serversLoaded, Response response) {
                    bus.post(serversLoaded);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.w(LOG_TAG, error);
                    if (error.getKind() == RetrofitError.Kind.HTTP) {
                        switch (error.getResponse().getStatus()) {
                            default:
                                bus.post(error);
                        }
                    }
                }
            });
        } else {
            bus.post(new NetworkDisabled());
        }
    }

    public void getServer(String server) {
        if (Net.checkConnection()) {
            api.getServer(server, new Callback<Server>() {
                @Override
                public void success(Server server, Response response) {
                    bus.post(server);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.w(LOG_TAG, error);
                    if (error.getKind() == RetrofitError.Kind.HTTP) {
                        switch (error.getResponse().getStatus()) {
                            case 400:
                                bus.post(new ServerNotExists());
                                break;
                            default:
                                bus.post(error);
                        }
                    }
                }
            });
        }
        else {
            bus.post(new NetworkDisabled());
        }
    }

    public void getManga(String server, String manga) {
        if (Net.checkConnection()) {
            api.getManga(server, manga, new Callback<Manga>() {
                @Override
                public void success(Manga manga, Response response) {
                    bus.post(manga);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.w(LOG_TAG, error);
                    if (error.getKind() == RetrofitError.Kind.HTTP) {
                        switch (error.getResponse().getStatus()) {
                            case 400:
                                bus.post(new MangaNotExists());
                                break;
                            default:
                                bus.post(error);
                        }
                    }
                }
            });
        }
        else {
            bus.post(new NetworkDisabled());
        }
    }

    public void getChapter(final String server_name, final String manga_name, final String chapter_name) {
        if (Net.checkConnection()) {
            api.getChapter(server_name, manga_name, chapter_name, new Callback<Chapter>() {
                @Override
                public void success(Chapter chapter, Response response) {
                    chapter.server_name = server_name;
                    chapter.manga_name = manga_name;
                    chapter.name = chapter_name;
                    bus.post(chapter);
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.w(LOG_TAG, error);
                    if (error.getKind() == RetrofitError.Kind.HTTP) {
                        switch (error.getResponse().getStatus()) {
                            case 400:
                                bus.post(new ChapterNotExists());
                                break;
                            default:
                                bus.post(error);
                        }
                    }
                }
            });
        }
        else {
            bus.post(new NetworkDisabled());
        }
    }

    public void getChapterRow(final String server_name, final String manga_name, final String chapter_name, final MangaListChaptersAdapter.ViewHolder holder, final Integer position) {
        if (Net.checkConnection()) {
            api.getChapter(server_name, manga_name, chapter_name, new Callback<Chapter>() {
                @Override
                public void success(Chapter chapter, Response response) {
                    chapter.server_name = server_name;
                    chapter.manga_name = manga_name;
                    chapter.name = chapter_name;
                    bus.post(new ChapterRow(chapter, holder, position));
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.w(LOG_TAG, error);
                    if (error.getKind() == RetrofitError.Kind.HTTP) {
                        switch (error.getResponse().getStatus()) {
                            case 400:
                                bus.post(new ChapterNotExists());
                                break;
                            default:
                                bus.post(error);
                        }
                    }
                }
            });
        }
        else {
            bus.post(new NetworkDisabled());
        }
    }
}
