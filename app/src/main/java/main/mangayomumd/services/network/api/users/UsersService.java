package main.mangayomumd.services.network.api.users;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.otto.Bus;

import java.lang.reflect.Modifier;

import main.mangayomumd.BuildConfig;
import main.mangayomumd.models.User;
import main.mangayomumd.services.ServiceBus;
import main.mangayomumd.services.network.api.utils.DefaultClient;
import main.mangayomumd.services.network.api.utils.WebServices;
import main.mangayomumd.services.network.api.users.events.Authenticated;
import main.mangayomumd.services.network.api.users.events.LogoutSuccessful;
import main.mangayomumd.services.network.api.users.events.TestSuccessfulLogin;
import main.mangayomumd.services.network.api.users.events.errors.EmailFormatInvalid;
import main.mangayomumd.services.network.api.users.events.errors.IncorrectParameters;
import main.mangayomumd.services.network.api.users.events.errors.PasswordNotMatch;
import main.mangayomumd.services.network.api.users.events.errors.UserAlreadyExists;
import main.mangayomumd.services.network.errors.UserNotExists;
import main.mangayomumd.services.network.errors.NetworkDisabled;
import main.mangayomumd.services.network.utils.Net;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.android.AndroidLog;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;
import retrofit.mime.TypedByteArray;

/**
 * Created by juanma on 30/10/15.
 */
public class UsersService {
    private final UsersAPI api;
    private final Bus bus;

    private final String LOG_TAG = UsersService.class.getSimpleName();

    private static UsersService _instance = null;

    public static UsersService getInstance() {
        if(_instance == null)
            _instance = getInstance(getDefaultApi(), getDefaultBus());
        return _instance;
    }

    /**
     * Method for Dependecy injection
     * @param api api interface to be used
     * @param bus bus to be used
     * @return The UsersService
     */
    public static UsersService getInstance(UsersAPI api, Bus bus) {
        _instance = new UsersService(api, bus);
        return _instance;
    }

    private static Bus getDefaultBus() {
        return ServiceBus.serviceBus;
    }

    private static Gson getDefaultGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        return builder.create();
    }

    private static UsersAPI getDefaultApi() {
        RestAdapter.Builder builder = new RestAdapter.Builder().setEndpoint(WebServices.USERS_SERVER);
        builder.setClient(DefaultClient.getDefaultClient());
        builder.setConverter(new GsonConverter(getDefaultGson()));
        if(BuildConfig.DEBUG)
            builder.setLogLevel(RestAdapter.LogLevel.FULL).setLog(new AndroidLog("RetrofitLog"));
        return builder.build().create(UsersAPI.class);
    }

    //Instance
    public UsersService(UsersAPI api, Bus bus) {
        this.api = api;
        this.bus = bus;
    }


    /*
    * Llamadas
    * */

    public void login(String user_name, String mail) {
        if (Net.checkConnection()) {
            api.login(user_name, mail, new Callback<User>() {
                @Override
                public void success(User user, Response response) {
                    bus.post(new Authenticated(user));
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.w(LOG_TAG, error);
                    if (error.getKind() == RetrofitError.Kind.HTTP) {
                        switch (error.getResponse().getStatus()) {
                            case 400:
                                bus.post(new UserNotExists());
                                break;
                            case 403:
                                bus.post(new UserAlreadyExists());
                                break;
                            case 500:
                                bus.post(new EmailFormatInvalid());
                                break;
                            default:
                                bus.post(error);
                        }
                    }
                }
            });
        }
        else {
            bus.post(new NetworkDisabled());
        }
    }

    public void testLogin(String client_id, String key) {
        if (Net.checkConnection()) {
            api.testLogin(client_id, key, new Callback<User>() {
                @Override
                public void success(User user, Response response) {
                    bus.post(new TestSuccessfulLogin(user));
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.w(LOG_TAG, error);
                    if (error.getKind() == RetrofitError.Kind.HTTP) {
                        switch (error.getResponse().getStatus()) {
                            case 400:
                                bus.post(new IncorrectParameters());
                                break;
                            case 401:
                                String json =  new String(((TypedByteArray)error.getResponse().getBody()).getBytes());
                                Log.w("PasswordNotMatch", json);
                                bus.post(new PasswordNotMatch());
                                break;
                            default:
                                bus.post(error);
                        }
                    }
                }
            });
        }
        else {
            bus.post(new NetworkDisabled());
        }
    }

    public void logout(String client_id, String key) {
        if (Net.checkConnection()) {
            api.logout(client_id, key, new Callback<User>() {
                @Override
                public void success(User user, Response response) {
                    bus.post(new LogoutSuccessful(user));
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.w(LOG_TAG, error);
                    if (error.getKind() == RetrofitError.Kind.HTTP) {
                        switch (error.getResponse().getStatus()) {
                            default:
                                bus.post(error);
                        }
                    }
                }
            });
        }
        else {
            bus.post(new NetworkDisabled());
        }
    }
}
