package main.mangayomumd.services.network.api.favourites.events;

import main.mangayomumd.models.User;

/**
 * Created by juanma on 31/10/15.
 */
public class ChapterModified {
    public String next_random;
    public String type;

    public ChapterModified(User user, String type) {
        this.next_random = user.next_random;
        this.type = type;
    }
}
