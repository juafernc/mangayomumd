package main.mangayomumd.services.network.api.favourites;

import java.util.List;

import main.mangayomumd.models.User;
import main.mangayomumd.services.network.api.favourites.events.ChaptersReadedLoaded;
import main.mangayomumd.services.network.api.favourites.events.FavouritesLoaded;
import retrofit.Callback;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by juanma on 31/10/15.
 */
public interface FavouritesAPI {
    String RESOURCE_FAVOURITE = "/favourite";

    @FormUrlEncoded
    @POST(FavouritesAPI.RESOURCE_FAVOURITE+"/{user}")
    void addFavourite(@Path("user") String user, @Field("client_id") String client_id, @Field("key") String key, @Field("server") String server, @Field("manga") String manga, @Field("chapter") List<String> chapter, Callback<User> cb);

    // TODO: implementar ETag
    @GET(FavouritesAPI.RESOURCE_FAVOURITE+"/{user}")
    void getFavourites(@Path("user") String user, @Query("client_id") String client_id, @Query("key") String key, @Query("addreaded") Boolean addreaded, @Query("addextrainfo") Boolean addextrainfo, Callback<FavouritesLoaded> cb);

    @DELETE(FavouritesAPI.RESOURCE_FAVOURITE+"/{user}/{server}/{manga}")
    void removeFavourite(@Path("user") String user, @Query("client_id") String client_id, @Query("key") String key, @Path("server") String server, @Path("manga") String manga, Callback<User> cb);

    // TODO: implementar ETag
    @GET(FavouritesAPI.RESOURCE_FAVOURITE+"/{user}/{server}/{manga}")
    void getChaptersReaded(@Path("user") String user, @Query("client_id") String client_id, @Query("key") String key, @Path("server") String server, @Path("manga") String manga, Callback<ChaptersReadedLoaded> cb);

    @FormUrlEncoded
    @POST(FavouritesAPI.RESOURCE_FAVOURITE+"/{user}/{server}/{manga}")
    void modifyChapterReaded(@Path("user") String user, @Field("client_id") String client_id, @Field("key") String key, @Path("server") String server, @Path("manga") String manga, @Field("chapter") List<String> chapter, @Field("type") String type, Callback<User> cb);
}
