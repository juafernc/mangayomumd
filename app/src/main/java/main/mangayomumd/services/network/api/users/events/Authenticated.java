package main.mangayomumd.services.network.api.users.events;

import main.mangayomumd.models.User;

/**
 * Created by juanma on 30/10/15.
 */
public class Authenticated {
    public String user_name;
    public String client_id;
    public String next_random;

    public Authenticated(User user) {
        this.user_name = user.user_name;
        this.client_id = user.client_id;
        this.next_random = user.next_random;
    }
}
