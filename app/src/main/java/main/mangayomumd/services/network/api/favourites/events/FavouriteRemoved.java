package main.mangayomumd.services.network.api.favourites.events;

import main.mangayomumd.models.User;

/**
 * Created by juanma on 31/10/15.
 */
public class FavouriteRemoved {
    public String next_random;

    public FavouriteRemoved(User user) {
        this.next_random = user.next_random;
    }
}
