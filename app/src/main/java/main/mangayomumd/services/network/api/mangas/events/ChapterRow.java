package main.mangayomumd.services.network.api.mangas.events;

import main.mangayomumd.models.Chapter;
import main.mangayomumd.views.adapters.MangaListChaptersAdapter;

/**
 * Created by juanma on 5/12/15.
 */
public class ChapterRow {
    public Chapter chapter;
    public MangaListChaptersAdapter.ViewHolder holder;
    public Integer position;

    public ChapterRow(Chapter chapter, MangaListChaptersAdapter.ViewHolder holder, Integer position) {
        this.chapter = chapter;
        this.holder = holder;
        this.position = position;
    }
}
