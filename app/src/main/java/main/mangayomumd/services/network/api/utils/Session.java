package main.mangayomumd.services.network.api.utils;

import main.mangayomumd.utils.Hash;

/**
 * Created by juanma on 31/10/15.
 */
public class Session {

    public static String getKey(String pin, String next_random) {
        return Hash.hashMD5(pin+next_random).toLowerCase();
    }

}
