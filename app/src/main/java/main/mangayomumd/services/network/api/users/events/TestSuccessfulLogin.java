package main.mangayomumd.services.network.api.users.events;

import main.mangayomumd.models.User;

/**
 * Created by juanma on 30/10/15.
 */
public class TestSuccessfulLogin {
    public String user_name;
    public String next_random;

    public TestSuccessfulLogin(User user) {
        this.user_name = user.user_name;
        this.next_random = user.next_random;
    }
}
