package main.mangayomumd.services.network.api.users.events;

import main.mangayomumd.models.User;

/**
 * Created by juanma on 30/10/15.
 */
public class LogoutSuccessful {
    public String user_name;

    public LogoutSuccessful(User user) {
        this.user_name = user.user_name;
    }
}
