package main.mangayomumd.services.network.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import main.mangayomumd.managers.App;

/**
 * Created by juanma on 30/10/15.
 */
public class Net {
    public static boolean checkConnection() {
        boolean bConectado = false;
        ConnectivityManager connec = (ConnectivityManager) App.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        // No sólo wifi, también GPRS
        NetworkInfo[] redes = connec.getAllNetworkInfo();
        for (int i = 0; i < 2; i++) {
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                bConectado = true;
            }
        }

        return bConectado;
    }
}
