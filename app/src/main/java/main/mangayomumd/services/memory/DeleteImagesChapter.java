package main.mangayomumd.services.memory;

import java.io.File;
import java.util.List;

import main.mangayomumd.managers.App;
import main.mangayomumd.utils.Re;

/**
 * Created by juanma on 29/11/15.
 */
public class DeleteImagesChapter {

    public static void delete(List<String> chapters) {
        for (String chapter : chapters) {
            new File(App.getAppContext().getFilesDir(), Re.getFichFromUrl(chapter)).delete();
        }
    }
}
