package main.mangayomumd.services;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;

/**
 * Created by juanma on 30/10/15.
 */
public class ServiceBus {
    public static final Bus serviceBus = new AndroidBus();

    private ServiceBus(){}

    public static class AndroidBus extends Bus {
        private final Handler handler = new Handler(Looper.getMainLooper());

        /*
            Always post to Main thread
         */
        @Override
        public void post(final Object event) {
            if(Looper.myLooper() == Looper.getMainLooper())
                super.post(event);
            else {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        AndroidBus.super.post(event);
                    }
                });
            }
        }
    }
}
