package main.mangayomumd.views.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import main.mangayomumd.R;
import main.mangayomumd.managers.BannerManager;
import main.mangayomumd.models.Manga;
import main.mangayomumd.models.Server;
import main.mangayomumd.services.ServiceBus;
import main.mangayomumd.services.listeners.RecyclerItemClickListener;
import main.mangayomumd.services.network.api.mangas.MangasService;
import main.mangayomumd.utils.Cte;
import main.mangayomumd.views.activities.MainActivity;
import main.mangayomumd.views.activities.MangaActivity;
import main.mangayomumd.views.adapters.ServerListMangasAdapter;
import retrofit.RetrofitError;

public class ServerListFragment extends Fragment implements RecyclerItemClickListener.OnItemClickListener {
    private OnFragmentInteractionListener mListener;

    private RecyclerView mRvMangas;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private TextView mTvNumMangas;

    Server server;

    public static ServerListFragment newInstance(Server server) {
        ServerListFragment fragment = new ServerListFragment();
        Bundle args = new Bundle();
        args.putParcelable(Cte.SERVER, server);
        fragment.setArguments(args);
        return fragment;
    }

    public ServerListFragment() {}

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        /*try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("testEficiencia", "time 1.1: " + String.valueOf(System.currentTimeMillis()));
        server = getArguments().getParcelable(Cte.SERVER);
        Log.d("testEficiencia", "time 1.2: " + String.valueOf(System.currentTimeMillis()));
        assert server != null;
        if (server != null)
            server.deserialize();
        Log.d("testEficiencia", "time 1.3: " + String.valueOf(System.currentTimeMillis()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_server_list, container, false);

        mRvMangas = (RecyclerView) view.findViewById(R.id.rv_list_mangas);
        //mAdapter = new ServerListMangasAdapter(getActivity(), R.layout.custom_list_manga_item, server.list_mangas);
        mAdapter = new ServerListMangasAdapter(getActivity(), R.layout.custom_list_manga_item, server.list_mangas);
        mRvMangas.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRvMangas.setLayoutManager(mLayoutManager);
        mRvMangas.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRvMangas, this));

        mTvNumMangas = (TextView) view.findViewById(R.id.tv_num_mangas);
        //mTvNumMangas.setText(getString(R.string.mangas_encontrados, server.list_mangas.size()));
        mTvNumMangas.setText(getString(R.string.mangas_encontrados, server.list_mangas.size()));

        BannerManager.createBanner(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(server.name);
        ((MainActivity)getActivity()).showProgress(false);
        ServiceBus.serviceBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ServiceBus.serviceBus.unregister(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(View view, int position) {
        Manga manga = Manga.getManga(server.name, server.list_mangas.get(position));
        if (manga == null) MangasService.getInstance().getManga(server.name, server.list_mangas.get(position));
        else MangaActivity.launch(getActivity(), manga);
        ((MainActivity)getActivity()).showProgress(true);
    }

    @Override
    public void onItemLongClick(View view, int position) {

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Subscribe
    public void getManga(Manga manga) {
        Log.d("testAPI", "getManga");
        Log.d("testAPI", manga.toString());
        manga.update();
        MangaActivity.launch(getActivity(), manga);
    }

    @Subscribe
    public void error(RetrofitError error) {
        Snackbar.make(mRvMangas, "Error con la red", Snackbar.LENGTH_LONG).setAction("Action", null).show();
    }

}
