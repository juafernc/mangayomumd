package main.mangayomumd.views.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import main.mangayomumd.R;
import main.mangayomumd.managers.BannerManager;
import main.mangayomumd.models.Manga;
import main.mangayomumd.services.listeners.RecyclerItemClickListener;
import main.mangayomumd.utils.Screen;
import main.mangayomumd.views.activities.MainActivity;
import main.mangayomumd.views.activities.MangaActivity;
import main.mangayomumd.views.adapters.FavouritesGridAdapter;

public class FavouritesFragment extends Fragment implements RecyclerItemClickListener.OnItemClickListener {
    private OnFragmentInteractionListener mListener;

    private RecyclerView mRvFavourites;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private List<Manga> listFavourites;

    public static FavouritesFragment newInstance() {
        return new FavouritesFragment();
    }

    public FavouritesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        /*try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favourites, container, false);

        mRvFavourites = (RecyclerView) view.findViewById(R.id.rv_favourites);
        mLayoutManager = new GridLayoutManager(getActivity(), Screen.numItemsCapacity(getActivity(), 320));
        mRvFavourites.setLayoutManager(mLayoutManager);
        mRvFavourites.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRvFavourites, this));

        BannerManager.createBanner(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        listFavourites = Manga.getFavourites();
        mAdapter = new FavouritesGridAdapter(R.layout.custom_grid_favourite_item, listFavourites);
        mRvFavourites.setAdapter(mAdapter);
        ((MainActivity)getActivity()).getSupportActionBar().setSubtitle(R.string.favoritos);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(View view, int position) {
        MangaActivity.launch(getActivity(), listFavourites.get(position));
    }

    @Override
    public void onItemLongClick(View view, int position) {
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

}
