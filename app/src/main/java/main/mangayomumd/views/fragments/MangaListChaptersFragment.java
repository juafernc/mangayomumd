package main.mangayomumd.views.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteFullException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Subscribe;

import main.mangayomumd.R;
import main.mangayomumd.models.Chapter;
import main.mangayomumd.models.Manga;
import main.mangayomumd.services.ServiceBus;
import main.mangayomumd.services.listeners.RecyclerItemClickListener;
import main.mangayomumd.services.network.api.mangas.events.ChapterRow;
import main.mangayomumd.utils.Cte;
import main.mangayomumd.views.activities.ChapterActivity;
import main.mangayomumd.views.adapters.MangaListChaptersAdapter;

public class MangaListChaptersFragment extends Fragment {

    private RecyclerView mRvChapters;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Manga manga;

    public static MangaListChaptersFragment newInstance(Manga manga) {
        MangaListChaptersFragment fragment = new MangaListChaptersFragment();
        Bundle args = new Bundle();
        args.putParcelable(Cte.MANGA, manga);
        fragment.setArguments(args);
        return fragment;
    }

    public MangaListChaptersFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manga = getArguments().getParcelable(Cte.MANGA);
        assert manga != null;
        if (manga != null)
            manga.deserialize();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manga_list_chapters, container, false);

        mRvChapters = (RecyclerView) view.findViewById(R.id.rv_list_chapters);
        mAdapter = new MangaListChaptersAdapter(getActivity(), R.layout.custom_list_chapter_item, manga);
        mRvChapters.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRvChapters.setLayoutManager(mLayoutManager);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ServiceBus.serviceBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        ServiceBus.serviceBus.unregister(this);
    }

    @Subscribe
    public void getChapter(ChapterRow chapterRow) {
        Chapter chapter = chapterRow.chapter;
        chapter.readed = false;
        chapter.downloaded = false;
        try {
            chapter.update();
        } catch (SQLiteFullException e) {
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Espacio insuficiente");
            alert.setMessage("Falta espacio en disco, por favor, libere espacio para poder realizar esta acción");
            alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alert.show();
        }
        Chapter chapterDB = Chapter.getChapter(chapter.server_name, chapter.manga_name, chapter.name);
        chapterDB.deserialize();
        ((MangaListChaptersAdapter) mAdapter).downloadChapter(chapterDB, chapterRow.holder, chapterRow.position);
    }
}
