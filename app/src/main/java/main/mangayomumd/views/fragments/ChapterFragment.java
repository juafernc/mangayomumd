package main.mangayomumd.views.fragments;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;

import main.mangayomumd.R;
import main.mangayomumd.services.network.utils.DownloadImage;
import main.mangayomumd.utils.Cte;
import main.mangayomumd.views.activities.ChapterActivity;
import main.mangayomumd.views.widgets.TouchImageView;

/**
 * Created by juanma on 5/11/15.
 */
public class ChapterFragment extends Fragment {

    private String image;
    private Boolean downloaded;
    private TouchImageView mTivImage;

    public static ChapterFragment newInstance(String image, Boolean downloaded) {
        ChapterFragment fragment = new ChapterFragment();
        Bundle args = new Bundle();
        args.putString(Cte.IMAGE, image);
        args.putBoolean(Cte.DOWNLOADED, downloaded);
        fragment.setArguments(args);
        return fragment;
    }

    public ChapterFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        image = getArguments().getString(Cte.IMAGE);
        downloaded = getArguments().getBoolean(Cte.DOWNLOADED);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_chapter, container, false);
        Log.d("image", image);
        Log.d("rotation", "ChapterFragment.onCreateView()");
        mTivImage = (TouchImageView) rootView.findViewById(R.id.tiv_image);
        setScaleType(getResources().getConfiguration());
        File file = downloaded? getActivity().getFilesDir() : getActivity().getCacheDir();
        DownloadImage downloadImage = new DownloadImage(file, image, mTivImage);
        downloadImage.download();
        mTivImage.setOnTouchListener(new View.OnTouchListener() {
            GestureDetector gd = new GestureDetector(new MyGestureDetector());

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("gesture", "click from image");
                return gd.onTouchEvent(event);
            }
        });
        return rootView;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setScaleType(newConfig);
    }

    public void setScaleType(Configuration config) {
        if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
            mTivImage.setScaleType(TouchImageView.ScaleType.CENTER_INSIDE);
        } else {
            mTivImage.setScaleType(TouchImageView.ScaleType.CENTER_CROP);
            //Log.d("scroll", "scroll position: " + mTivImage.getVerticalScrollbarPosition());
            //Log.d("scroll", "scroll position antes: " + mTivImage.getScrollPosition());
            //mTivImage.setScrollPosition(0.0f, 0.0f);
            //Log.d("scroll", "scroll position después: " + mTivImage.getScrollPosition());
        }
    }

    public class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent event) {
            int x;
            int margenDer, margenIzq;
            x = (int) event.getX();

            Log.d("Gesture", "MyGesture");

            DisplayMetrics display = getResources().getDisplayMetrics();
            margenDer = (int) (display.widthPixels / 1.3);
            margenIzq = display.widthPixels - margenDer;
            ViewPager mViewPager = ((ChapterActivity)getActivity()).mViewPager;
            if(x <= margenIzq){
                mViewPager.setCurrentItem(mViewPager.getCurrentItem()-1);
            }
            else if(x >= margenDer){
                mViewPager.setCurrentItem(mViewPager.getCurrentItem()+1);
            }
            else {
                long time = System.currentTimeMillis();
                if ( (time - ((ChapterActivity)getActivity()).currentTime) > ((ChapterActivity)getActivity()).AUTO_HIDE_DELAY_MILLIS ) {
                    ((ChapterActivity) getActivity()).showToolbar(true);
                    ((ChapterActivity)getActivity()).currentTime = time;
                }
                else {
                    ((ChapterActivity) getActivity()).showToolbar(false);
                    ((ChapterActivity)getActivity()).currentTime = 0;
                }
            }

            return false;
        }
    }

}
