package main.mangayomumd.views.fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import main.mangayomumd.R;
import main.mangayomumd.models.Chapter;
import main.mangayomumd.views.adapters.ChaptersDownloadedAdapter;
import main.mangayomumd.views.adapters.MangaListChaptersAdapter;

public class ChaptersDownloadedFragment extends Fragment {

    private RecyclerView mRvChapters;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private List<Chapter> chapters;

    public static ChaptersDownloadedFragment newInstance() {
        return new ChaptersDownloadedFragment();
    }

    public ChaptersDownloadedFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        chapters = Chapter.getChaptersDownloaded();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chapters_downloaded, container, false);

        mRvChapters = (RecyclerView) view.findViewById(R.id.rv_list_chapters);
        mAdapter = new ChaptersDownloadedAdapter(getActivity(), R.layout.custom_list_chapter_item, chapters);
        mRvChapters.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRvChapters.setLayoutManager(mLayoutManager);

        return view;
    }

}
