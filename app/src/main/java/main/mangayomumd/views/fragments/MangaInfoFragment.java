package main.mangayomumd.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import main.mangayomumd.R;
import main.mangayomumd.models.Manga;
import main.mangayomumd.utils.Cte;

public class MangaInfoFragment extends Fragment {

    private ImageView mIvManga;
    private TextView mTvTitle;
    private TextView mTvAuthor;
    private TextView mTvChapters;
    private TextView mTvRanking;
    private TextView mTvDescription;

    private Manga manga;

    public static MangaInfoFragment newInstance(Manga manga) {
        MangaInfoFragment fragment = new MangaInfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(Cte.MANGA, manga);
        fragment.setArguments(args);
        return fragment;
    }

    public MangaInfoFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manga = getArguments().getParcelable(Cte.MANGA);
        assert manga != null;
        if (manga != null)
            manga.deserialize();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manga_info, container, false);

        mIvManga = (ImageView) view.findViewById(R.id.iv_manga);
        mTvTitle = (TextView) view.findViewById(R.id.tv_title_manga);
        mTvAuthor = (TextView) view.findViewById(R.id.tv_author_manga);
        mTvChapters = (TextView) view.findViewById(R.id.tv_chapters_manga);
        mTvRanking = (TextView) view.findViewById(R.id.tv_clasification_manga);
        mTvDescription = (TextView) view.findViewById(R.id.tv_description_manga);

        if (manga != null) {
            Log.d("test", "imagen: " + manga.url_image);
            if (manga.url_image != null)
                Picasso.with(getActivity()).load(manga.url_image).centerInside().resize(150, 220).onlyScaleDown().placeholder(R.color.purple100).error(R.drawable.sin_imagen).into(mIvManga);
            else
                Picasso.with(getActivity()).load(R.drawable.sin_imagen).centerCrop().resize(150, 220).onlyScaleDown().into(mIvManga);
            mTvTitle.setText(manga.name);
            mTvAuthor.setText("Unknown");
            mTvChapters.setText(String.valueOf(manga.list_chapters.size()));
            mTvRanking.setText(manga.ranking.toString());
            mTvDescription.setText(manga.description);
        }

        return view;
    }

}
