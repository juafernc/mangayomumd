package main.mangayomumd.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import main.mangayomumd.R;
import main.mangayomumd.managers.SharedPreferencesManager;
import main.mangayomumd.models.Manga;
import main.mangayomumd.utils.Cte;
import main.mangayomumd.views.adapters.MangaPagerAdapter;

public class MangaActivity extends AppCompatActivity {

    private MangaPagerAdapter mMangaPagerAdapter;

    private ViewPager mViewPager;

    private Manga manga;

    public static void launch(Activity activity, Manga manga) {
        Intent intent = new Intent(activity, MangaActivity.class);
        intent.putExtra(Cte.MANGA, manga);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manga);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            manga = bundle.getParcelable(Cte.MANGA);
            if (manga != null) {
                SharedPreferencesManager.getInstance().setString(Cte.SERVER, manga.server_name);
                SharedPreferencesManager.getInstance().setString(Cte.MANGA, manga.name);
            }
        }
        else {
            String server_name = SharedPreferencesManager.getInstance().getString(Cte.SERVER);
            String manga_name = SharedPreferencesManager.getInstance().getString(Cte.MANGA);
            manga = Manga.getManga(server_name, manga_name);
        }
        if (manga != null)
            manga.deserialize();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.mangayomu_neg);

        if (manga != null) {
            getSupportActionBar().setSubtitle(manga.name);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mMangaPagerAdapter = new MangaPagerAdapter(getSupportFragmentManager(), manga);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mMangaPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        final ImageButton mIbtnStartReadManga = (ImageButton) findViewById(R.id.ibtn_startReadManga);
        mIbtnStartReadManga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        final ImageButton mIbtnMarkFavourite = (ImageButton) findViewById(R.id.ibtn_markToFavourite);
        if (manga != null) {
            if (manga.favourite == null) mIbtnMarkFavourite.setImageResource(android.R.drawable.star_big_off);
            else mIbtnMarkFavourite.setImageResource(manga.favourite ? android.R.drawable.star_big_on : android.R.drawable.star_big_off);
        }
        mIbtnMarkFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (manga.favourite == null) manga.favourite = true;
                else manga.favourite = !manga.favourite;
                mIbtnMarkFavourite.setImageResource(manga.favourite ? android.R.drawable.star_big_on : android.R.drawable.star_big_off);
                manga.update();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_manga, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
