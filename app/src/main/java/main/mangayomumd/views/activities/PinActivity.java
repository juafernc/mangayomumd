package main.mangayomumd.views.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.squareup.otto.Subscribe;

import main.mangayomumd.R;
import main.mangayomumd.managers.SharedPreferencesManager;
import main.mangayomumd.services.ServiceBus;
import main.mangayomumd.services.network.api.users.UsersService;
import main.mangayomumd.services.network.api.users.events.TestSuccessfulLogin;
import main.mangayomumd.services.network.api.users.events.errors.IncorrectParameters;
import main.mangayomumd.services.network.api.users.events.errors.PasswordNotMatch;
import main.mangayomumd.services.network.api.utils.Session;
import main.mangayomumd.utils.Cte;
import main.mangayomumd.utils.Validate;
import retrofit.RetrofitError;

public class PinActivity extends AppCompatActivity {

    private EditText mEtPin;
    private Button mBtnPin;

    private View mProgressView;
    private View mLoginFormView;

    private String pin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mProgressView = findViewById(R.id.login_progress);
        mLoginFormView = findViewById(R.id.login_form);

        mEtPin = (EditText) findViewById(R.id.et_pin);
        mBtnPin = (Button) findViewById(R.id.btn_pin);
        mBtnPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ServiceBus.serviceBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ServiceBus.serviceBus.unregister(this);
    }

    private void attemptLogin() {
        // Reset errors.
        mEtPin.setError(null);

        // Store values at the time of the login attempt.
        pin = mEtPin.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(pin)) {
            mEtPin.setError(getString(R.string.error_field_required));
            focusView = mEtPin;
            cancel = true;
        } else if (!isPinValid(pin)) {
            mEtPin.setError(getString(R.string.error_invalid_email));
            focusView = mEtPin;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            String client_id = SharedPreferencesManager.getInstance().getString(Cte.CLIENT_ID);
            String next_random = SharedPreferencesManager.getInstance().getString(Cte.NEXT_RANDOM);
            String key = Session.getKey(pin, next_random);
            if (next_random != null && !next_random.isEmpty() && key != null && !key.isEmpty())
                UsersService.getInstance().testLogin(client_id, key);
            else {
                showProgress(false);
                Snackbar.make(mLoginFormView, getString(R.string.error_missing_data_pin), Snackbar.LENGTH_LONG).setAction("Action", null).show();
            }
        }
    }

    private boolean isPinValid(String pin) {
        return Validate.pin(pin);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     *
     * Respuestas servidor
     * */

    @Subscribe
    public void pin(TestSuccessfulLogin testSuccessfulLogin) {
        Log.d("LoginPin", "Login correcto: " + testSuccessfulLogin.next_random);
        SharedPreferencesManager.getInstance().setString(Cte.NEXT_RANDOM, testSuccessfulLogin.next_random);
        SharedPreferencesManager.getInstance().setString(Cte.USER_NAME, testSuccessfulLogin.user_name);
        SharedPreferencesManager.getInstance().setString(Cte.PIN, pin);
        SharedPreferencesManager.getInstance().setBoolean(Cte.LOGIN, true);
        finish();
    }

    @Subscribe
    public void error(IncorrectParameters incorrectParameters) {
        showProgress(false);
        Log.d("RetrofitError", "IncorrectParameters");
    }

    @Subscribe
    public void error(PasswordNotMatch passwordNotMatch) {
        showProgress(false);
        Log.d("RetrofitError", "PasswordNotMatch");
    }

    @Subscribe
    public void error(RetrofitError error) {
        showProgress(false);
        Log.d("RetrofitError", "RetrofitError");
    }
}
