package main.mangayomumd.views.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import main.mangayomumd.R;
import main.mangayomumd.managers.SharedPreferencesManager;
import main.mangayomumd.models.Chapter;
import main.mangayomumd.models.Manga;
import main.mangayomumd.models.Server;
import main.mangayomumd.services.ServiceBus;
import main.mangayomumd.services.network.api.favourites.FavouritesService;
import main.mangayomumd.services.network.api.favourites.events.FavouriteAdded;
import main.mangayomumd.services.network.api.favourites.events.FavouritesLoaded;
import main.mangayomumd.services.network.api.mangas.MangasService;
import main.mangayomumd.services.network.api.mangas.events.ServersLoaded;
import main.mangayomumd.services.network.api.mangas.events.errors.ChapterNotExists;
import main.mangayomumd.services.network.api.mangas.events.errors.MangaNotExists;
import main.mangayomumd.services.network.api.mangas.events.errors.ServerNotExists;
import main.mangayomumd.services.network.api.users.events.Authenticated;
import main.mangayomumd.services.network.api.users.events.LogoutSuccessful;
import main.mangayomumd.services.network.api.users.events.TestSuccessfulLogin;
import main.mangayomumd.services.network.api.users.events.errors.IncorrectParameters;
import main.mangayomumd.services.network.api.users.events.errors.PasswordNotMatch;
import main.mangayomumd.services.network.api.users.events.errors.UserAlreadyExists;
import main.mangayomumd.services.network.api.utils.Session;
import main.mangayomumd.services.network.errors.UserNotExists;
import main.mangayomumd.utils.Cte;
import main.mangayomumd.views.fragments.ChaptersDownloadedFragment;
import main.mangayomumd.views.fragments.FavouritesFragment;
import main.mangayomumd.views.fragments.ServerListFragment;
import retrofit.RetrofitError;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    // Tiempo que debe pasar antes de cerrar la aplicación
    private final int TIME_TO_CLOSE_APP = 2000;
    private long currentTime;

    private ProgressBar progress;
    private View flameLayout;

    private NavigationView navigationView;
    private View headerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.mipmap.mangayomu_neg);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_favourites);

        headerView = navigationView.inflateHeaderView(R.layout.nav_header_main);
        headerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                if (SharedPreferencesManager.getInstance().getBoolean(Cte.LOGIN)) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                    dialog.setTitle(getString(R.string.alert_dialog_logout_title));
                    dialog.setMessage(getString(R.string.alert_dialog_logout_message));
                    dialog.setCancelable(true);
                    dialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferencesManager.getInstance().setBoolean(Cte.LOGIN, false);
                            setLogin();
                            dialog.dismiss();
                        }
                    });
                    dialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
                else {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                }

            }
        });

        progress = (ProgressBar) findViewById(R.id.progressBar);
        flameLayout = findViewById(R.id.fl_main);

        Log.d("test", "savedInstanceState: " + savedInstanceState);

        if (savedInstanceState == null) {
            int view_id = SharedPreferencesManager.getInstance().getInteger(Cte.VISTA_MENU_PRINCIPAL);
            Server server;
            switch (view_id) {
                default:
                case R.id.nav_favourites:
                    getFragmentManager().beginTransaction().replace(R.id.fl_main, FavouritesFragment.newInstance()).commit();
                    getSupportActionBar().setSubtitle(getString(R.string.favoritos));
                    break;
                case R.id.nav_downloads:
                    getFragmentManager().beginTransaction().replace(R.id.fl_main, ChaptersDownloadedFragment.newInstance()).commit();
                    getSupportActionBar().setSubtitle(getString(R.string.descargados));
                    break;
                case R.id.nav_animextremist:
                    server = Server.getServer(getString(R.string.animextremist));
                    getFragmentManager().beginTransaction().replace(R.id.fl_main, ServerListFragment.newInstance(server)).commit();
                    getSupportActionBar().setSubtitle(server.name);
                    break;
                case R.id.nav_baroto_es:
                    server = Server.getServer(getString(R.string.batoto_es));
                    getFragmentManager().beginTransaction().replace(R.id.fl_main, ServerListFragment.newInstance(server)).commit();
                    getSupportActionBar().setSubtitle(server.name);
                    break;

                case R.id.nav_mangahere_es:
                    server = Server.getServer(getString(R.string.mangaHere_es));
                    getFragmentManager().beginTransaction().replace(R.id.fl_main, ServerListFragment.newInstance(server)).commit();
                    getSupportActionBar().setSubtitle(server.name);
                    break;
                case R.id.nav_mangareader:
                    server = Server.getServer(getString(R.string.mangareader));
                    getFragmentManager().beginTransaction().replace(R.id.fl_main, ServerListFragment.newInstance(server)).commit();
                    getSupportActionBar().setSubtitle(server.name);
                    break;
                case R.id.nav_submanga:
                    server = Server.getServer(getString(R.string.submanga));
                    getFragmentManager().beginTransaction().replace(R.id.fl_main, ServerListFragment.newInstance(server)).commit();
                    getSupportActionBar().setSubtitle(server.name);
                    break;
            }
            //SharedPreferencesManager.getInstance().setInteger(Cte.VISTA_MENU_PRINCIPAL, R.id.nav_favourites);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showProgress(false);
        ServiceBus.serviceBus.register(this);
        currentTime = System.currentTimeMillis();
        setLogin();
        //UsersService.getInstance().login("Noexisto", "juanmafn@gmail.com");
        //UsersService.getInstance().testLogin("5634eb4d1053ba271700a3dd", Session.getKey("276660", "bwckymYavMudIDSBXnX3Tg=="));
        //UsersService.getInstance().logout("563489021053ba271700a3dc", Session.getKey("614750", "KGX99NkPrc+l7qsJVw/i6g=="));
        //MangasService.getInstance().getServers();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ServiceBus.serviceBus.unregister(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if (SharedPreferencesManager.getInstance().getInteger(Cte.VISTA_MENU_PRINCIPAL) != R.id.nav_favourites) {
            navigationView.setCheckedItem(R.id.nav_favourites);
            SharedPreferencesManager.getInstance().setInteger(Cte.VISTA_MENU_PRINCIPAL, R.id.nav_favourites);
            getFragmentManager().beginTransaction().replace(R.id.fl_main, FavouritesFragment.newInstance()).commit();
            getSupportActionBar().setSubtitle(getString(R.string.favoritos));
        }
        else {
            long ct = System.currentTimeMillis();
            if( (ct - currentTime) < TIME_TO_CLOSE_APP ) {
                super.onBackPressed();
            }
            else {
                currentTime = ct;
                Snackbar.make(flameLayout, getString(R.string.alert_time_to_clouse_app), Snackbar.LENGTH_SHORT).setAction("Action", null).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        else if (id == R.id.action_pin) {
            startActivity(new Intent(this, PinActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        final Server server;

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        SharedPreferencesManager.getInstance().setInteger(Cte.VISTA_MENU_PRINCIPAL, id);

        switch (id) {
            case R.id.nav_favourites:
                getFragmentManager().beginTransaction().replace(R.id.fl_main, FavouritesFragment.newInstance()).commit();
                break;
            case R.id.nav_downloads:
                getFragmentManager().beginTransaction().replace(R.id.fl_main, ChaptersDownloadedFragment.newInstance()).commit();
                getSupportActionBar().setSubtitle(getString(R.string.descargados));
                break;
            case R.id.nav_animextremist:
                showProgress(true);
                server = Server.getServer(getString(R.string.animextremist));
                if (server == null) MangasService.getInstance().getServer(getString(R.string.animextremist));
                else getFragmentManager().beginTransaction().replace(R.id.fl_main, ServerListFragment.newInstance(server)).commit();
                getSupportActionBar().setSubtitle(getString(R.string.animextremist));
                break;
            case R.id.nav_baroto_es:
                showProgress(true);
                server = Server.getServer(getString(R.string.batoto_es));
                if (server == null) MangasService.getInstance().getServer(getString(R.string.batoto_es));
                else getFragmentManager().beginTransaction().replace(R.id.fl_main, ServerListFragment.newInstance(server)).commit();
                getSupportActionBar().setSubtitle(getString(R.string.batoto_es));
                break;

            case R.id.nav_mangahere_es:
                showProgress(true);
                server = Server.getServer(getString(R.string.mangaHere_es));
                if (server == null) MangasService.getInstance().getServer(getString(R.string.mangaHere_es));
                else getFragmentManager().beginTransaction().replace(R.id.fl_main, ServerListFragment.newInstance(server)).commit();
                getSupportActionBar().setSubtitle(getString(R.string.mangaHere_es));
                break;
            case R.id.nav_mangareader:
                showProgress(true);
                server = Server.getServer(getString(R.string.mangareader));
                if (server == null) MangasService.getInstance().getServer(getString(R.string.mangareader));
                else getFragmentManager().beginTransaction().replace(R.id.fl_main, ServerListFragment.newInstance(server)).commit();
                getSupportActionBar().setSubtitle(getString(R.string.mangareader));
                break;
            case R.id.nav_submanga:
                Log.d("testEficiencia", "time 0.1: " + String.valueOf(System.currentTimeMillis()));
                showProgress(true);
                server = Server.getServer(getString(R.string.submanga));
                if (server == null) MangasService.getInstance().getServer(getString(R.string.submanga));
                else {
                    getFragmentManager().beginTransaction().replace(R.id.fl_main, ServerListFragment.newInstance(server)).commit();
                }
                getSupportActionBar().setSubtitle(getString(R.string.submanga));
                Log.d("testEficiencia", "time 0.2: " + String.valueOf(System.currentTimeMillis()));
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setLogin() {
        TextView mTvUserName = (TextView) headerView.findViewById(R.id.tv_user_name);
        TextView mTvEmail = (TextView) headerView.findViewById(R.id.tv_email);
        ImageView mIvAvatar = (ImageView) headerView.findViewById(R.id.iv_avatar);
        String email = SharedPreferencesManager.getInstance().getString(Cte.EMAIL);
        String username = SharedPreferencesManager.getInstance().getString(Cte.USER_NAME);
        String image = SharedPreferencesManager.getInstance().getString(Cte.USER_IMAGE);
        boolean isLogin = SharedPreferencesManager.getInstance().getBoolean(Cte.LOGIN);
        if (isLogin && !email.isEmpty() && !username.isEmpty()) {
            mTvUserName.setText(username);
            mTvEmail.setText(email);
            if (!image.isEmpty()) {
                Picasso.with(this).load(image).into(mIvAvatar);
            }
        }
        else {
            mTvUserName.setText(getString(R.string.title_activity_login));
            mTvEmail.setText("");
        }
    }

    public void showProgress(boolean show) {
        progress.setVisibility(show ? View.VISIBLE : View.GONE);
        flameLayout.setVisibility(show ? View.GONE : View.VISIBLE);
    }


    /**
     * LLAMADAS AL SERVIDOR
     * */

    @Subscribe
    public void getServer(Server server) {
        Log.d("testAPI", "getServer");
        Log.d("testAPI", server.toString());
        server.update();
        getFragmentManager().beginTransaction().replace(R.id.fl_main, ServerListFragment.newInstance(server)).commit();
        //showProgress(false);
    }

    /**
     *
     * TESTING
     *
     * */

    @Subscribe
    public void login(Authenticated authenticated) {
        Log.d("testAPI", "Authenticated");
        Log.d("testAPI", authenticated.user_name);
        Log.d("testAPI", authenticated.client_id);
        Log.d("testAPI", authenticated.next_random);
        SharedPreferencesManager.getInstance().setString("next_random", authenticated.next_random);
    }

    @Subscribe
    public void testSuccessfulLogin(TestSuccessfulLogin testSuccessfulLogin) {
        Log.d("testAPI", "TestSuccessfulLogin");
        Log.d("testAPI", "user_name: " + testSuccessfulLogin.user_name);
        Log.d("testAPI", "next_random: " + testSuccessfulLogin.next_random);
        SharedPreferencesManager.getInstance().setString("next_random", testSuccessfulLogin.next_random);
        FavouritesService.getInstance().getFavourites(testSuccessfulLogin.user_name, "5634eb4d1053ba271700a3dd", Session.getKey("276660", testSuccessfulLogin.next_random), false, false);
    }

    @Subscribe
    public void logout(LogoutSuccessful logoutSuccessful) {
        Log.d("testAPI", "LogoutSuccessful");
    }

    @Subscribe
    public void getServers(ServersLoaded serversLoaded) {
        Log.d("testAPI", "getServers");
        Log.d("testAPI", serversLoaded.list_servers.toString());
        MangasService.getInstance().getServer(serversLoaded.list_servers.get(3));
    }

    @Subscribe
    public void getManga(Manga manga) {
        Log.d("testAPI", "getManga");
        Log.d("testAPI", manga.toString());
        //MangasService.getInstance().getChapter(manga.server_name, manga.name, manga.list_chapters.get(0));
    }

    @Subscribe
    public void getChapter(Chapter chapter) {
        Log.d("testAPI", "getChapter");
        Log.d("testAPI", chapter.toString());
    }

    @Subscribe
    public void addFavourite(FavouriteAdded favouriteAdded) {
        Log.d("testAPI", "favouriteAdded");
        Log.d("testAPI", "next_random: "+favouriteAdded.next_random);
    }

    @Subscribe
    public void getFavourites(FavouritesLoaded favouritesLoaded) {
        Log.d("testAPI", "getFavourites");
        Log.d("testAPI", "next_random: " + favouritesLoaded.next_random);
        Log.d("testAPI", "list_mangas: " + favouritesLoaded.list_mangas.toString());
        SharedPreferencesManager.getInstance().setString("next_random", favouritesLoaded.next_random);
    }

    @Subscribe
    public void userNotExists(UserNotExists userNotExists) {
        Log.d("testAPI", "UserNotExists");
    }

    @Subscribe
    public void userAlreadyExists(UserAlreadyExists userAlreadyExists) {
        Log.d("testAPI", "UserAlreadyExists");
    }

    @Subscribe
    public void incorrectParameters(IncorrectParameters incorrectParameters) {
        Log.d("testAPI", "IncorrectParameters");
    }

    @Subscribe
    public void passwordNotMatch(PasswordNotMatch passwordNotMatch) {
        Log.d("testAPI", "PasswordNotMatch");
    }

    @Subscribe
    public void serverNotExists(ServerNotExists serverNotExists) {
        Log.d("testAPI", "ServerNotExists");
    }

    @Subscribe
    public void mangaNotExists(MangaNotExists mangaNotExists) {
        Log.d("testAPI", "MangaNotExists");
    }

    @Subscribe
    public void chapterNotExists(ChapterNotExists chapterNotExists) {
        Log.d("testAPI", "ChapterNotExists");
    }

    @Subscribe
    public void error(RetrofitError error) {
        Log.d("testAPI", "Error desconocido");
    }
}
