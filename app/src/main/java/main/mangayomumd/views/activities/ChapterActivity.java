package main.mangayomumd.views.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import java.io.File;
import java.util.List;

import main.mangayomumd.R;
import main.mangayomumd.models.Chapter;
import main.mangayomumd.services.ServiceBus;
import main.mangayomumd.services.network.api.mangas.MangasService;
import main.mangayomumd.services.network.utils.DownloadImage;
import main.mangayomumd.utils.Cte;
import main.mangayomumd.views.adapters.ChapterPagerAdapter;

public class ChapterActivity extends AppCompatActivity {

    public static final int AUTO_HIDE_DELAY_MILLIS = 6000;
    public long currentTime;

    private Toolbar toolbar;
    private ProgressBar progressBar;
    private Boolean downloadComplete = false;

    private ChapterPagerAdapter mChapterPagerAdapter;
    public ViewPager mViewPager;
    private Chapter chapter;
    private TextView mTvPage;

    public static void launch(Activity activity, Chapter chapter) {
        Intent intent = new Intent(activity, ChapterActivity.class);
        intent.putExtra(Cte.CHAPTER, chapter);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapter);
        initView();
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        //delayedHide(AUTO_HIDE_DELAY_MILLIS);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ServiceBus.serviceBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ServiceBus.serviceBus.unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chapters, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    void initView() {
        chapter = getIntent().getExtras().getParcelable(Cte.CHAPTER);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((TextView) findViewById(R.id.tv_title_manga)).setText(chapter.manga_name);
        ((TextView) findViewById(R.id.tv_title_chapter)).setText(chapter.name);

        mTvPage = (TextView) findViewById(R.id.tv_page);

        mViewPager = (ViewPager) findViewById(R.id.container);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        showToolbar(true);

        if (chapter.listImagesString != null) {
            chapter.deserialize();
            completeView();
        }
        else {
            MangasService.getInstance().getChapter(chapter.server_name, chapter.manga_name, chapter.name);
        }

        currentTime = System.currentTimeMillis();
    }

    void completeView() {
        mChapterPagerAdapter = new ChapterPagerAdapter(getSupportFragmentManager(), chapter);

        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mChapterPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mTvPage.setText((mViewPager.getCurrentItem() + 1) + "/" + chapter.list_images.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        mTvPage.setText("1/" + chapter.list_images.size());
        progressBar.setMax(chapter.list_images.size());
        DownloadChapter downloadChapter = new DownloadChapter(chapter.list_images);
        downloadChapter.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void showToolbar(boolean show) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            Animation animation = AnimationUtils.loadAnimation(this, show ? R.anim.show : R.anim.hide);
            if (show) {
                actionBar.show();
                progressBar.setVisibility(downloadComplete ? View.GONE : View.VISIBLE);
                mViewPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            else {
                resetDelayHide();
                actionBar.hide();
                progressBar.setVisibility(View.GONE);
                mViewPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
            }
            toolbar.startAnimation(animation);
            if (!downloadComplete) progressBar.startAnimation(animation);
        }
    }

    private final Handler mHideHandler = new Handler();
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            showToolbar(false);
        }
    };

    public void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    public void resetDelayHide() {
        mHideHandler.removeCallbacks(mHideRunnable);
    }

    public class DownloadChapter extends AsyncTask<Void, Integer, Integer> {

        List<String> images;

        public DownloadChapter(List<String> images) {
            this.images = images;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            int i = 1;
            File file = chapter.downloaded ? getFilesDir() : getCacheDir();
            for (String image : images) {
                DownloadImage downloadImage = new DownloadImage(file, image);
                downloadImage.download();
                publishProgress(i++);
                Log.d("download", "image " + (i - 1) + " descarcaga");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            downloadComplete = true;
            progressBar.setVisibility(View.GONE);
        }
    }

    /**
     *
     * RESPUESTAS DEL SERVIDOR
     * */

    @Subscribe
    public void getChapter(Chapter chapter) {
        if (this.chapter.readed != null) chapter.readed = this.chapter.readed;
        else chapter.readed = false;
        if (this.chapter.downloaded != null) chapter.downloaded = this.chapter.downloaded;
        else chapter.downloaded = false;
        chapter.update();
        this.chapter = chapter;
        completeView();
    }

}
