package main.mangayomumd.views.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import main.mangayomumd.R;

/**
 * Created by juanma on 2/11/15.
 */
public class ServerListMangasAdapter extends RecyclerView.Adapter<ServerListMangasAdapter.ViewHolder> {

    private Activity activity;
    private int layoutResId;
    private List<String> mangas;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mIvManga;
        public TextView mTvTitleManga;
        public TextView mTvAuthorManga;

        public ViewHolder(View view) {
            super(view);
            mIvManga = (ImageView) view.findViewById(R.id.iv_manga);
            mTvTitleManga = (TextView) view.findViewById(R.id.tv_title_manga);
            mTvAuthorManga = (TextView) view.findViewById(R.id.tv_author_manga);
        }
    }

    public ServerListMangasAdapter(Activity activity, int layoutResId, List<String> mangas) {
        this.activity = activity;
        this.layoutResId = layoutResId;
        this.mangas = mangas;
        if (mangas == null) {
            mangas = new ArrayList<String>();
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Picasso.with(activity).load(R.drawable.sin_imagen).centerCrop().resize(60, 60).onlyScaleDown().into(holder.mIvManga);
        holder.mTvTitleManga.setText(mangas != null ? mangas.get(position): "");
        holder.mTvAuthorManga.setText("Unknown");
    }

    @Override
    public int getItemCount() {
        return mangas.size();
    }
}
