package main.mangayomumd.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import main.mangayomumd.models.Chapter;
import main.mangayomumd.views.fragments.ChapterFragment;

/**
 * Created by juanma on 5/11/15.
 */
public class ChapterPagerAdapter extends FragmentPagerAdapter {

    private Chapter chapter;

    public ChapterPagerAdapter(FragmentManager fm, Chapter chapter) {
        super(fm);
        this.chapter = chapter;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return ChapterFragment.newInstance(chapter.list_images.get(position), chapter.downloaded);
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return chapter.list_images.size();
    }
}
