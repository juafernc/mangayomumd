package main.mangayomumd.views.adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteFullException;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.HashMap;

import main.mangayomumd.R;
import main.mangayomumd.managers.SharedPreferencesManager;
import main.mangayomumd.models.Chapter;
import main.mangayomumd.models.Manga;
import main.mangayomumd.services.memory.DeleteImagesChapter;
import main.mangayomumd.services.network.api.mangas.MangasService;
import main.mangayomumd.services.network.utils.DownloadImage;
import main.mangayomumd.services.network.utils.Net;
import main.mangayomumd.views.activities.ChapterActivity;

/**
 * Created by juanma on 2/11/15.
 */
public class MangaListChaptersAdapter extends RecyclerView.Adapter<MangaListChaptersAdapter.ViewHolder> {

    private Activity activity;
    private int layoutResId;
    private Manga manga;
    private HashMap<Integer, Chapter> chapters;

    HashMap<Integer, DownloadChapter> chapterDownload;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View mRlRow;
        public ProgressBar mProgressBarChapter;
        public ImageButton mIbtnDownloadChapter;
        public TextView mTvChapter;
        public ImageButton mIbtnChapterViewed;
        public ProgressBar mProgressBar;

        public ViewHolder(View view) {
            super(view);
            mRlRow = view.findViewById(R.id.rl_chapter_item);
            mProgressBarChapter = (ProgressBar) view.findViewById(R.id.progressbar_chapter);
            mIbtnDownloadChapter = (ImageButton) view.findViewById(R.id.ibtn_download_chapter);
            mTvChapter = (TextView) view.findViewById(R.id.tv_chapter);
            mIbtnChapterViewed = (ImageButton) view.findViewById(R.id.ibtn_chapter_viewed);
            mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        }
    }

    public MangaListChaptersAdapter(Activity activity, int layoutResId, Manga manga) {
        this.activity = activity;
        this.layoutResId = layoutResId;
        this.manga = manga;
        chapterDownload = new HashMap<>();
        chapters = new HashMap<>();
        new GetChaptersDataBase(manga).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // Obtenemos el capítulo, y si no existe en la base de datos lo creamos
        Chapter chapterDB = chapters.get(position);
        if (chapterDB == null) {
            chapterDB = Chapter.getChapter(manga.server_name, manga.name, manga.list_chapters.get(position));
            if (chapterDB == null) {
                chapterDB = new Chapter(manga.list_chapters.get(position), manga.server_name, manga.name, false, false);
                chapterDB.save();
            }
            if (chapterDB.listImagesString != null) {
                chapterDB.deserialize();
            }
            chapters.put(position, chapterDB);
        }
        final Chapter chapter = chapterDB;

        // Asignamos elementos gráficos a cada fila
        loadViews(chapter, holder, position);

        // Añadimos listener para el botón de capítulo leído
        holder.mIbtnChapterViewed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setChapterViewedButton(chapter, holder);
            }
        });

        // Añadimos listener para el botón de descarga de capítulo
        holder.mIbtnDownloadChapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadChapterButton(chapter, holder, position);
            }
        });

        // Añadimos listener a la fila para acceder al capítulo
        holder.mRlRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewChapter(chapter, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (manga != null && manga.list_chapters != null)
            return manga.list_chapters.size();
        else
            return 0;
    }

    public void loadViews(Chapter chapter, ViewHolder holder, Integer position) {
        // Asignamos imagen al botón de descargar capítulo en función de si está descargado o no
        if (chapter.downloaded != null && chapter.downloaded)
            holder.mIbtnDownloadChapter.setImageResource(android.R.drawable.ic_menu_delete);
        else
            holder.mIbtnDownloadChapter.setImageResource(android.R.drawable.stat_sys_download);

        // Asignamos imagen al botón de capítulo leído en función de si está leído o no
        if (chapter.readed != null)
            holder.mIbtnChapterViewed.setImageResource(chapter.readed ? R.drawable.readed : R.drawable.unreaded);
        else
            holder.mIbtnChapterViewed.setImageResource(R.drawable.unreaded);

        // Asignamos el nombre del capítulo
        holder.mTvChapter.setText(manga.list_chapters.get(position));

        // Mostramos el ProgressBar si lo requiere
        DownloadChapter dc = chapterDownload.get(position);
        if (dc != null) {
            holder.mProgressBar.setVisibility(View.VISIBLE);
            Log.d("download", "progress: " + dc.getProgress() + "/" + chapter.list_images.size());
            holder.mProgressBar.setMax(chapter.list_images != null ? chapter.list_images.size() : 0);
            holder.mProgressBar.setProgress(dc.getProgress() != null ? dc.getProgress() : 0);

            holder.mIbtnDownloadChapter.setImageResource(android.R.drawable.ic_menu_close_clear_cancel);
            holder.mIbtnDownloadChapter.setVisibility(View.VISIBLE);
            holder.mProgressBarChapter.setVisibility(View.GONE);

            dc.updateHolder(holder);
        }
        else if (SharedPreferencesManager.getInstance().getBoolean(chapter.getIdString())) {
            downloadChapter(chapter, holder, position);
        }
        else {
            holder.mProgressBarChapter.setVisibility(View.GONE);
            holder.mProgressBar.setVisibility(View.GONE);
            holder.mIbtnDownloadChapter.setVisibility(View.VISIBLE);
        }
    }

    public void setChapterViewedButton(Chapter chapter, ViewHolder holder) {
        if (chapter.readed) {
            holder.mIbtnChapterViewed.setImageResource(R.drawable.unreaded);
            chapter.readed = false;
        }
        else {
            holder.mIbtnChapterViewed.setImageResource(R.drawable.readed);
            chapter.readed = true;
        }
        chapter.save();
    }

    public void downloadChapterButton(Chapter chapter, ViewHolder holder, Integer position) {
        DownloadChapter dc = chapterDownload.get(position);
        if (dc != null) {
            SharedPreferencesManager.getInstance().setBoolean(chapter.getIdString(), false);
            dc.cancel(true);
            deleteChapter(chapter, holder);
            holder.mIbtnDownloadChapter.setVisibility(View.VISIBLE);
            holder.mProgressBar.setVisibility(View.GONE);
            dc = null;
            chapterDownload.put(position, null);
        }
        else {
            if (chapter.listImagesString != null) {
                chapter.deserialize();
                if (chapter.downloaded) {
                    deleteChapter(chapter, holder);
                } else {
                    downloadChapter(chapter, holder, position);
                }
            } else {
                holder.mIbtnDownloadChapter.setVisibility(View.GONE);
                holder.mProgressBarChapter.setVisibility(View.VISIBLE);
                MangasService.getInstance().getChapterRow(manga.server_name, manga.name, manga.list_chapters.get(position), holder, position);
            }
        }
    }

    public void viewChapter(Chapter chapter, Integer position) {
        if (chapter != null) {
            ChapterActivity.launch(activity, chapter);
        }
        else {
            chapter = new Chapter(manga.list_chapters.get(position), manga.server_name, manga.name, false, false);
            ChapterActivity.launch(activity, chapter);
        }
    }

    public void downloadChapter(Chapter chapter, ViewHolder holder, Integer position) {
        chapters.put(position, chapter);
        DownloadChapter downloadChapter = new DownloadChapter(chapter, holder, position);
        downloadChapter.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        chapterDownload.put(position, downloadChapter);
    }

    public void deleteChapter(Chapter chapter, ViewHolder holder) {
        DeleteImagesChapter.delete(chapter.list_images);
        holder.mIbtnDownloadChapter.setImageResource(android.R.drawable.stat_sys_download);
        chapter.downloaded = false;
        chapter.save();
    }

    public class DownloadChapter extends AsyncTask<Void, Integer, Integer> {

        Chapter chapter;
        ViewHolder holder;
        Integer position;
        Integer progress;

        public DownloadChapter(Chapter chapter, ViewHolder holder, Integer position) {
            this.chapter = chapter;
            this.holder = holder;
            this.position = position;
        }

        public Integer getProgress() {
            return progress;
        }

        public void updateHolder(ViewHolder holder) {
            this.holder = holder;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*if (chapter == null || chapter.list_images == null) {
                chapter = Chapter.getChapter()
            }*/
            if (chapter.list_images == null) {
                if (chapter.listImagesString != null)
                    chapter.deserialize();
                else
                    cancel(true);
            }
            Log.d("download", "chapter: " + chapter);
            Log.d("download", "chapter.list: " + chapter.list_images);
            if (position == holder.getLayoutPosition()) {
                holder.mProgressBarChapter.setVisibility(View.GONE);
                holder.mIbtnDownloadChapter.setImageResource(android.R.drawable.ic_menu_close_clear_cancel);
                holder.mIbtnDownloadChapter.setVisibility(View.VISIBLE);
                holder.mProgressBar.setMax(chapter.list_images.size());
                progress=0;
                holder.mProgressBar.setProgress(progress);
                holder.mProgressBar.setVisibility(View.VISIBLE);
            }
            SharedPreferencesManager.getInstance().setBoolean(chapter.getIdString(), true);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progress = values[0];
            if (position == holder.getLayoutPosition()) {
                holder.mProgressBar.setMax(chapter.list_images.size());
                holder.mProgressBar.setProgress(progress);
            }
            Log.d("download", "onProgressUpdate() -> TextView: " + holder.mTvChapter.getText().toString());
        }

        @Override
        protected Integer doInBackground(Void... params) {
            int i = 0;
            for (String image : chapter.list_images) {
                if (Net.checkConnection()) {
                    DownloadImage downloadImage = new DownloadImage(activity.getFilesDir(), image);
                    downloadImage.download();
                    publishProgress(i++);
                }
                else {
                    cancel(true);
                    return null;
                }
                Log.d("download", "image " + i + " descarcagada");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            if (position == holder.getLayoutPosition()) {
                holder.mIbtnDownloadChapter.setImageResource(android.R.drawable.ic_menu_delete);
                holder.mIbtnDownloadChapter.setVisibility(View.VISIBLE);
                holder.mProgressBarChapter.setVisibility(View.GONE);
                holder.mProgressBar.setVisibility(View.GONE);
            }
            SharedPreferencesManager.getInstance().setBoolean(chapter.getIdString(), false);
            chapterDownload.put(position, null);
            chapter.downloaded = true;
            try {
                chapter.save();
            } catch (SQLiteFullException e) {
                AlertDialog.Builder alert = new AlertDialog.Builder(activity);
                alert.setTitle("Espacio insuficiente");
                alert.setMessage("Falta espacio en disco, por favor, libere espacio para poder realizar esta acción");
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

            if (position == holder.getLayoutPosition()) {
                holder.mIbtnDownloadChapter.setImageResource(android.R.drawable.stat_sys_download);
                holder.mIbtnDownloadChapter.setVisibility(View.VISIBLE);
                holder.mProgressBarChapter.setVisibility(View.GONE);
                holder.mProgressBar.setVisibility(View.GONE);
            }

            if (!Net.checkConnection()) {
                AlertDialog.Builder alert = new AlertDialog.Builder(activity);
                alert.setTitle("Internet");
                alert.setMessage("No hay conexión a internet");
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        }
    }

    public class GetChaptersDataBase extends AsyncTask<Void, Void, Void> {

        Manga manga;

        public GetChaptersDataBase(Manga manga) {
            this.manga = manga;
        }

        @Override
        protected Void doInBackground(Void... params) {
            Chapter chapter;
            for (int i=0; i<manga.list_chapters.size(); ++i) {
                chapter = Chapter.getChapter(manga.server_name, manga.name, manga.list_chapters.get(i));
                if (chapter == null) {
                    chapter = new Chapter(manga.list_chapters.get(i), manga.server_name, manga.name, false, false);
                    chapter.save();
                }
                chapters.put(i, chapter);
                if (chapter == null) Log.d("database", "chapter " + manga.list_chapters.get(i) + " is null");
                else Log.d("database", "get " + i + ": " + chapter.name + " -> " + chapter.list_images + " -> " + chapter.listImagesString);
            }
            return null;
        }
    }

}
