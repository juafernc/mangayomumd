package main.mangayomumd.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import main.mangayomumd.R;
import main.mangayomumd.managers.App;
import main.mangayomumd.models.Manga;
import main.mangayomumd.services.network.utils.DownloadImage;

/**
 * Created by juanma on 1/11/15.
 */
public class FavouritesGridAdapter extends RecyclerView.Adapter<FavouritesGridAdapter.ViewHolder> {
    private int layoutResId;
    private List<Manga> listFavourites;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mIvManga;
        public TextView mTvMangaName;
        public TextView mTvServerName;

        public ViewHolder(View view) {
            super(view);
            mIvManga = (ImageView) view.findViewById(R.id.iv_manga);
            mTvMangaName = (TextView) view.findViewById(R.id.tv_manga_name);
            mTvServerName = (TextView) view.findViewById(R.id.tv_server_name);
        }
    }


    public FavouritesGridAdapter(int layoutResId, List<Manga> listFavourites) {
        this.layoutResId = layoutResId;
        this.listFavourites = listFavourites;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        DownloadImage downloadImage = new DownloadImage(App.getAppContext().getFilesDir(), listFavourites.get(position).url_image, holder.mIvManga);
        downloadImage.download();
        holder.mTvMangaName.setText(listFavourites.get(position).name);
        holder.mTvServerName.setText(listFavourites.get(position).server_name);
    }

    @Override
    public int getItemCount() {
        return listFavourites.size();
    }

}
