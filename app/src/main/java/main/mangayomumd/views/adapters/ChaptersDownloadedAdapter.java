package main.mangayomumd.views.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import main.mangayomumd.R;
import main.mangayomumd.models.Chapter;
import main.mangayomumd.services.memory.DeleteImagesChapter;
import main.mangayomumd.views.activities.ChapterActivity;

/**
 * Created by juanma on 5/12/15.
 */
public class ChaptersDownloadedAdapter extends RecyclerView.Adapter<ChaptersDownloadedAdapter.ViewHolder> {

    private Activity activity;
    private int layoutResId;
    private List<Chapter> chapters;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View mRlRow;
        public ImageButton mIbtnDownloadChapter;
        public TextView mTvChapter;
        public ImageButton mIbtnChapterViewed;

        public ViewHolder(View view) {
            super(view);
            mRlRow = view.findViewById(R.id.rl_chapter_item);
            mIbtnDownloadChapter = (ImageButton) view.findViewById(R.id.ibtn_download_chapter);
            mTvChapter = (TextView) view.findViewById(R.id.tv_chapter);
            mIbtnChapterViewed = (ImageButton) view.findViewById(R.id.ibtn_chapter_viewed);
        }
    }

    public ChaptersDownloadedAdapter(Activity activity, int layoutResId, List<Chapter> chapters) {
        this.activity = activity;
        this.layoutResId = layoutResId;
        this.chapters = chapters;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutResId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        loadViews(holder, position);

        // Añadimos listener para el botón de capítulo leído
        holder.mIbtnChapterViewed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setChapterViewedButton(chapters.get(position), holder);
            }
        });

        // Añadimos listener para el botón de descarga de capítulo
        holder.mIbtnDownloadChapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteChapter(chapters.get(position), holder);
                chapters.remove(position);
                notifyDataSetChanged();
            }
        });

        // Añadimos listener a la fila para acceder al capítulo
        holder.mRlRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewChapter(chapters.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (chapters != null)
            return chapters.size();
        else return 0;
    }

    public void loadViews(ViewHolder holder, Integer position) {
        // Asignamos imagen al botón para borrar capítulo
        holder.mIbtnDownloadChapter.setImageResource(android.R.drawable.ic_menu_delete);

        // Asignamos imagen al botón de capítulo leído en función de si está leído o no
        if (chapters.get(position).readed != null)
            holder.mIbtnChapterViewed.setImageResource(chapters.get(position).readed ? R.drawable.readed : R.drawable.unreaded);
        else
            holder.mIbtnChapterViewed.setImageResource(R.drawable.unreaded);

        // Asignamos el nombre del capítulo
        holder.mTvChapter.setText(chapters.get(position).manga_name + "\n" + chapters.get(position).name + "\n" + chapters.get(position).server_name);
    }

    public void setChapterViewedButton(Chapter chapter, ViewHolder holder) {
        if (chapter.readed) {
            holder.mIbtnChapterViewed.setImageResource(R.drawable.unreaded);
            chapter.readed = false;
        }
        else {
            holder.mIbtnChapterViewed.setImageResource(R.drawable.readed);
            chapter.readed = true;
        }
        chapter.save();
    }

    public void deleteChapter(Chapter chapter, ViewHolder holder) {
        chapter.deserialize();
        DeleteImagesChapter.delete(chapter.list_images);
        holder.mIbtnDownloadChapter.setImageResource(android.R.drawable.stat_sys_download);
        chapter.downloaded = false;
        chapter.save();
    }

    public void viewChapter(Chapter chapter) {
        ChapterActivity.launch(activity, chapter);
    }

}
