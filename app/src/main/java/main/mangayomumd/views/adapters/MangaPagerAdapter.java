package main.mangayomumd.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import main.mangayomumd.R;
import main.mangayomumd.managers.App;
import main.mangayomumd.models.Manga;
import main.mangayomumd.views.fragments.MangaInfoFragment;
import main.mangayomumd.views.fragments.MangaListChaptersFragment;

/**
 * Created by juanma on 2/11/15.
 */
public class MangaPagerAdapter extends FragmentPagerAdapter {

    private Manga manga;

    public MangaPagerAdapter(FragmentManager fm, Manga manga) {
        super(fm);
        this.manga = manga;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) return MangaInfoFragment.newInstance(manga);
        else return MangaListChaptersFragment.newInstance(manga);
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return App.getAppContext().getString(R.string.tab_info);
            case 1:
                return App.getAppContext().getString(R.string.tab_chapters);
        }
        return null;
    }
}
