package main.mangayomumd.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by juanma on 22/11/15.
 */
public class Validate {
    private static final String PATTERN_MAIL = "^[-\\w\\+]+(\\.[-\\w\\+]+)*@[-\\w]+(\\.[-\\w]+)*(\\.[A-Za-z]{2,})$";
    private static final String PATTERN_PIN = "[0-9]{6}";

    static public boolean email(String email) {
        try {
            Pattern pattern = Pattern.compile(PATTERN_MAIL);
            Matcher matcher = pattern.matcher(email);
            return matcher.matches();
        } catch (RuntimeException e) {
            return false;
        }
    }

    static public boolean pin(String pin) {
        try {
            Pattern pattern = Pattern.compile(PATTERN_PIN);
            Matcher matcher = pattern.matcher(pin);
            return matcher.matches();
        } catch (RuntimeException e) {
            return false;
        }
    }
}
