package main.mangayomumd.utils;

import android.app.Activity;
import android.graphics.Point;
import android.util.Log;

/**
 * Created by juanma on 2/11/15.
 */
public class Screen {
    public static int numItemsCapacity(Activity activity, int width) {
        Point size = new Point();
        activity.getWindow().getWindowManager().getDefaultDisplay().getSize(size);
        double density = size.x/(double)width;
        double decimal = (density - (int) density);
        Log.d("screen", "x: " + size.x + ", density: " + size.x/(double)width + ", decimal: " + decimal +  ", trunk: " + Math.round(size.x/(double)width));
        if (decimal < 0.7) return (int) (size.x/(double)width);
        else return (int) Math.round(size.x/(double)width);
    }
}
