package main.mangayomumd.utils;

import android.util.Log;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by juanma on 31/10/15.
 */
public class Hash {
    private static byte[] getHash(String password, String type) {
        MessageDigest digest=null;
        try {
            digest = MessageDigest.getInstance(type);
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        digest.reset();
        return digest.digest(password.getBytes());
    }

    private static String bin2hex(byte[] data) {
        return String.format("%0" + (data.length*2) + "X", new BigInteger(1, data));
    }

    public static String hash256(String cadena) {
        return bin2hex(getHash(cadena, "SHA-256"));
    }

    public static String hashMD5(String cadena) {
        return bin2hex(getHash(cadena, "MD5"));
    }
}
