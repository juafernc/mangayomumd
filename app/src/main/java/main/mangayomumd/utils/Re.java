package main.mangayomumd.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by juanma on 9/11/15.
 */
public class Re {
    public static String getFichFromUrl(String url) {
        Pattern pattern = Pattern.compile("[0-9a-zA-Z]+");
        Matcher matcher = pattern.matcher(url);
        String res = "";
        while (matcher.find()) {
            res += matcher.group(0);
        }
        return res;
    }
}
