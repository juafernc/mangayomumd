package main.mangayomumd.utils;

/**
 * Created by juanma on 5/11/15.
 */
public class Cte {

    // SharePreferents
    // TODO:

    // Paso de variables
    public static String SERVER = "server";
    public static String MANGA = "manga";
    public static String CHAPTER = "chapter";
    public static String IMAGE = "image";
    public static String DOWNLOADED = "downloaded";

    // Variables de control
    public static String VISTA_MENU_PRINCIPAL = "vista_menu_principal";

    // Variables de usuario
    public static String LOGIN = "login";
    public static String CLIENT_ID = "client_id";
    public static String NEXT_RANDOM = "next_random";
    public static String PIN = "pin";
    public static String USER_NAME = "user_name";
    public static String EMAIL = "email";
    public static String USER_IMAGE = "user_image";

}
