package main.mangayomumd;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import main.mangayomumd.models.Chapter;
import main.mangayomumd.models.Manga;
import main.mangayomumd.models.Server;
import main.mangayomumd.services.network.api.mangas.MangasService;
import main.mangayomumd.services.network.api.mangas.events.ServersLoaded;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by juanma on 31/10/15.
 */
@RunWith(AndroidJUnit4.class)
public class MangasAPITest {

    /**
     *
     * TESTING GET_SERVERS
     * */

    @Test
    public void llamadaGetServersCorrecta() {
        final boolean[] running = {true};
        MangasService.getInstance().getApi().getServers(new Callback<ServersLoaded>() {
            @Override
            public void success(ServersLoaded serversLoaded, Response response) {
                Assert.assertTrue(true);
                running[0] = false;
            }

            @Override
            public void failure(RetrofitError error) {
                Assert.assertTrue(false);
                running[0] = false;
            }
        });

        try {
            while (running[0]) {
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void llamadaGetServersDevuelveServidores() {
        final boolean[] running = {true};
        MangasService.getInstance().getApi().getServers(new Callback<ServersLoaded>() {
            @Override
            public void success(ServersLoaded serversLoaded, Response response) {
                Assert.assertTrue(serversLoaded.list_servers.size() > 0);
                running[0] = false;
            }

            @Override
            public void failure(RetrofitError error) {
                Assert.assertTrue(false);
                running[0] = false;
            }
        });

        try {
            while (running[0]) {
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void llamadaGetServersDevuelve5Servidores() {
        final boolean[] running = {true};
        MangasService.getInstance().getApi().getServers(new Callback<ServersLoaded>() {
            @Override
            public void success(ServersLoaded serversLoaded, Response response) {
                Assert.assertTrue(serversLoaded.list_servers.size() == 5);
                running[0] = false;
            }

            @Override
            public void failure(RetrofitError error) {
                Assert.assertTrue(false);
                running[0] = false;
            }
        });

        try {
            while (running[0]) {
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void llamadaGetServersDevuelveLosServidoresSiguientes() {
        final List<String> servers = Arrays.asList(new String[]{"Animextremist", "Submanga", "MangaHere ES", "MangaReader", "Batoto ES"});
        final boolean[] running = {true};
        MangasService.getInstance().getApi().getServers(new Callback<ServersLoaded>() {
            @Override
            public void success(ServersLoaded serversLoaded, Response response) {
                boolean correcto = true;
                for (String server : serversLoaded.list_servers) {
                    if (!servers.contains(server)) {
                        correcto = false;
                        break;
                    }
                }
                Assert.assertTrue(correcto);
                running[0] = false;
            }

            @Override
            public void failure(RetrofitError error) {
                Assert.assertTrue(false);
                running[0] = false;
            }
        });

        try {
            while (running[0]) {
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /***********************************************************************************************************************/

    /**
     *
     * TESTING GET_MANGA
     * */

    @Test
    public void llamadaGetServerMangaReaderCorrecta() {
        final boolean[] running = {true};
        MangasService.getInstance().getApi().getServer("MangaReader", new Callback<Server>() {
            @Override
            public void success(Server server, Response response) {
                Assert.assertTrue(true);
                running[0] = false;
            }

            @Override
            public void failure(RetrofitError error) {
                Assert.assertTrue(false);
                running[0] = false;
            }
        });

        try {
            while (running[0]) {
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void llamadaGetServerMangaReaderContieneMangas() {
        final boolean[] running = {true};
        MangasService.getInstance().getApi().getServer("MangaReader", new Callback<Server>() {
            @Override
            public void success(Server server, Response response) {
                Assert.assertTrue(server.list_mangas.size() > 0);
                running[0] = false;
            }

            @Override
            public void failure(RetrofitError error) {
                Assert.assertTrue(false);
                running[0] = false;
            }
        });

        try {
            while (running[0]) {
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void llamadaGetMangaMangaReaderOnePieceContieneCapitulos() {
        final boolean[] running = {true};
        MangasService.getInstance().getApi().getManga("MangaReader", "One Piece", new Callback<Manga>() {
            @Override
            public void success(Manga manga, Response response) {
                Assert.assertTrue(manga.list_chapters.size() > 0);
                running[0] = false;
            }

            @Override
            public void failure(RetrofitError error) {
                Assert.assertTrue(false);
                running[0] = false;
            }
        });

        try {
            while (running[0]) {
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     *
     * TESTING GET_CHAPTER
     * */

    @Test
    public void llamadaGetChapterCorrecta() {
        final boolean[] running = {true};
        MangasService.getInstance().getApi().getChapter("MangaReader", "One Piece", "One Piece 800", new Callback<Chapter>() {
            @Override
            public void success(Chapter chapter, Response response) {
                Assert.assertTrue(true);
                running[0] = false;
            }

            @Override
            public void failure(RetrofitError error) {
                Assert.assertTrue(false);
                running[0] = false;
            }
        });

        try {
            while (running[0]) {
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void llamadaGetChapterMangaReaderOnePieceCapitulo800ContieneImagenes() {
        final boolean[] running = {true};
        MangasService.getInstance().getApi().getChapter("MangaReader", "One Piece", "One Piece 800", new Callback<Chapter>() {
            @Override
            public void success(Chapter chapter, Response response) {
                Assert.assertTrue(chapter.list_images.size() > 0);
                running[0] = false;
            }

            @Override
            public void failure(RetrofitError error) {
                Assert.assertTrue(false);
                running[0] = false;
            }
        });

        try {
            while (running[0]) {
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
